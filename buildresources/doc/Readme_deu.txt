EPISIM Simulator version 1.5.2 ist eine Software, 
welche erlaubt Zellverhalten in einem multizellulären Gewebe 
auf Basis eines Multiagentensystems zu simulieren.

Einige Features des EPISIM Simulators:
- Einfache und intuitive Benutzungsoberfläche.
- Import von Zellverhaltensmodellen
- Simulation von Zellverhaltensmodellen
- Simulation von SBML Modellen mit Hilfe von COPASI
- Überwachung der Simulationsergebnisse mittels Diagrammen und
  vorverarbeiteten Datenexporten

Hardware- und Softwareanforderungen:
- Betriebssystem: für Version 1.5.2 Windows 98/2000/XP/Vista/7/8/10


Contact:
Thomas Sütterlin
Nationales Zentrum für Tumorerkrankungen
Hamamatsu TIGA Center
Universitätsklinikum Heidelberg
thomas.suetterlin@bioquant.uni-heidelberg.de

Niels Grabe
Nationales Zentrum für Tumorerkrankungen
Hamamatsu TIGA Center
Universitätsklinikum Heidelberg
niels.grabe@bioquant.uni-heidelberg.de

Heidelberg, 2016
