EPISIM Simulator version 1.5.2 is a software  that enables 
users to simulate cellular behavior in a multicellular tissue
using a multiagent-based simulation approach.

Some feature of the EPISIM Simulator:
- Easy to use customizable graphical user interface
- Import of Cell Behavioral Models
- Multiagent Simulation of Cell Behavioral Models
- Simulation of SBML Models using COPASI
- Monitoring of the simulation results with flexible charts
  and data exports

Requirements:
- Operating system for version 1.5.2: Windows 98/2000/XP/Vista/7/8/10

Contact:
Thomas Sütterlin
National Center for Tumor Diseases
Hamamatsu TIGA Center
University Hospital Heidelberg
thomas.suetterlin@bioquant.uni-heidelberg.de

Niels Grabe
National Center for Tumor Diseases
Hamamatsu TIGA Center
niels.grabe@bioquant.uni-heidelberg.de

Heidelberg, 2016
