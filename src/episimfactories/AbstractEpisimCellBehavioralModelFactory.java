package episimfactories;

import episiminterfaces.*;

public abstract class AbstractEpisimCellBehavioralModelFactory {
	
	
	public abstract EpisimCellBehavioralModelGlobalParameters getEpisimCellBehavioralModelGlobalParametersObject();
	
	
	public abstract Class getEpisimCellBehavioralModelClass();
	
	

}
