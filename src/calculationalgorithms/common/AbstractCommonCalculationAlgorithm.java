package calculationalgorithms.common;

import sim.app.episim.model.AbstractCell;
import sim.app.episim.util.EnhancedSteppable;
import sim.app.episim.util.GenericBag;



public abstract class AbstractCommonCalculationAlgorithm {
	protected GenericBag<AbstractCell> allCells;
	public void registerCells(GenericBag<AbstractCell> allCells){
		this.allCells = allCells;		
	}
	
	public void newSimStep(){ }
	
}
