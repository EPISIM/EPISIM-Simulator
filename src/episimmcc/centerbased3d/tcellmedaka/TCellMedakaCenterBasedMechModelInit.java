package episimmcc.centerbased3d.tcellmedaka;

/*
 * Sets model initial conditions
 * * Initial cell position (calculated with icosahedral mesh)
 * * Differentiation levels
 * * Initial parameter values
 */

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import ec.util.MersenneTwisterFast;
import sim.app.episim.EpisimExceptionHandler;
import sim.app.episim.model.UniversalCell;
import sim.app.episim.model.biomechanics.centerbased3d.tcellmedaka.TCellMedakaCenterBased3DModel;
import sim.app.episim.model.biomechanics.centerbased3d.tcellmedaka.TCellMedakaCenterBased3DModelGP;
import sim.app.episim.model.controller.ModelController;
import sim.app.episim.model.controller.TissueController;
import sim.app.episim.model.initialization.BiomechanicalModelInitializer;
import sim.app.episim.model.misc.MiscalleneousGlobalParameters;
import sim.app.episim.model.misc.MiscalleneousGlobalParameters.MiscalleneousGlobalParameters3D;
import sim.app.episim.persistence.SimulationStateData;
import sim.app.episim.visualization.threedim.ContinuousCellFieldPortrayal3D;
import sim.util.Double3D;
import episiminterfaces.EpisimBiomechanicalModel;
import episiminterfaces.EpisimCellBehavioralModelGlobalParameters;
import episiminterfaces.EpisimDifferentiationLevel;
import episiminterfaces.EpisimPortrayal;
import episimmcc.EpisimModelConnector;


public class TCellMedakaCenterBasedMechModelInit extends BiomechanicalModelInitializer {

    // Fields
	SimulationStateData simulationStateData = null;
	private static double CELL_WIDTH = 0;
	private static double CELL_HEIGHT = 0;
	private static double CELL_LENGTH = 0;

	// Default constructor
	public TCellMedakaCenterBasedMechModelInit() 
	{
	    super(); // --> BiomechanicalModelInitializer; super constructs null object
		TissueController.getInstance().getTissueBorder().loadNoMembrane(); // this model has no basement membrane object
		
		// Load global parameters
		MiscalleneousGlobalParameters param = MiscalleneousGlobalParameters.getInstance(); 
		if(param instanceof MiscalleneousGlobalParameters3D)
		{
		    ((MiscalleneousGlobalParameters3D)param).setStandardMembrane_2_Dim_Gauss(false);
			((MiscalleneousGlobalParameters3D)param).setOptimizedGraphics(false);
		}
	}

	// Constructor when loading from saved snapshot
    public TCellMedakaCenterBasedMechModelInit(SimulationStateData simulationStateData) 
    {
		super(simulationStateData);
		this.simulationStateData = simulationStateData;
	}
	
	// Implementation of abstract method buildStandardInitialCellEnsemble in BiomechanicalModelInitializer
    // Create the distribution of cells in the initial condition of the simulation
	protected ArrayList<UniversalCell> buildStandardInitialCellEnsemble() 
	{
		// Load the default values for cell dimensions from the model at runtime.
        // The variables "WIDTH_DEFAULT", "HEIGHT_DEFAULT", and "LENGTH_DEFAULT" need to be declared in EPISIM Modeller.
        EpisimCellBehavioralModelGlobalParameters cbGP = ModelController.getInstance().getEpisimCellBehavioralModelGlobalParameters();		
		try
		{
	        Field field = cbGP.getClass().getDeclaredField("WIDTH_DEFAULT");
	        CELL_WIDTH = field.getDouble(cbGP);  
	        field = cbGP.getClass().getDeclaredField("HEIGHT_DEFAULT");
	        CELL_HEIGHT = field.getDouble(cbGP);
	        field = cbGP.getClass().getDeclaredField("LENGTH_DEFAULT");
	        CELL_LENGTH = field.getDouble(cbGP);   
        }
        catch (NoSuchFieldException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }
        catch (SecurityException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }
        catch (IllegalArgumentException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }
        catch (IllegalAccessException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }

		// Get the biomechanical global parameters
		TCellMedakaCenterBased3DModelGP mechModelGP = (TCellMedakaCenterBased3DModelGP) ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters();
			
		HashSet<Point3d> existingCoordinates = new HashSet<Point3d>();
		ArrayList<UniversalCell> standardCellEnsemble = new ArrayList<UniversalCell>();

		final MersenneTwisterFast random_uniform = new MersenneTwisterFast(System.currentTimeMillis());
		final MersenneTwisterFast fixed_seed_rand = new MersenneTwisterFast(mechModelGP.getInitialization_seed());
		
		double domainWidth = mechModelGP.getWidthInMikron(); // x
		double domainHeight = mechModelGP.getHeightInMikron(); // y
		double domainLength = mechModelGP.getLengthInMikron(); // z
		
		double thymusCenterX = mechModelGP.getThymusCenterX();
 		double thymusCenterY = mechModelGP.getThymusCenterY();
 		double thymusCenterZ = mechModelGP.getThymusCenterZ();
 		double thymusWidth = mechModelGP.getThymusWidth();
 		double thymusHeight = mechModelGP.getThymusHeight();
 		double thymusLength = mechModelGP.getThymusLength();
 		
 		double sizeTEC = 2*mechModelGP.getRadiusTEC();
 		double sizeTSP_source = 2*mechModelGP.getRadiusTSP_source();
 		double sizeTSP = 2*mechModelGP.getRadiusTSP();
 		
 		double medullaFactor = mechModelGP.getThymusInnerCoreFraction();
 		double migrationConeHeight = mechModelGP.getMigrationConeHeight();

    	double domainBorderFraction = 0.025; // buffer zone at the edges of the domain where no cells are instantiated
    	double x = 0;
    	double y = 0;
    	double z = 0;
    	
    	// For each cell type, generate random coordinates, check if they're inside the admissible volumes, regenerate if they're not.
		// 0 = default; 1 = TEC soma; 2 = TEC arm; 3 = TSP source; 4 = TSP; 5 = ab; 6 = gd; 7 = ab mature; 8 = gd mature
    	
    	boolean pseudo2D = mechModelGP.getPseudo2D();
    	boolean mammalTypeSource = mechModelGP.getMammalTypeSource();
    	
    	if (pseudo2D) 
    	{ // flat ellipse slice
    		
     		int nShells = mechModelGP.getTec_nShells();
     		int shellSubs = mechModelGP.getTec_shellSubdivisions();
     		
     		double centralExclusionFraction = mechModelGP.getTec_centralExclusionFraction();
     		double shellSpacingFactor = mechModelGP.getTec_ShellRadiusFactor();
     		double angleFactor = mechModelGP.getTec_SubdivisionFactor();
     		
     		int iterLim = 15;
     		int id = -1;

     		for ( int curShell = 0; curShell < nShells; curShell++ )
    	    {
     			int curShellSubs = shellSubs*(1+curShell);
     			double angleStep = (Math.PI/shellSubs)/(1+curShell);
    	    	
    	    	for ( int curShellSub = 0; curShellSub < curShellSubs; curShellSub++ )
    	    	{
    	    		id += 1;
        	    	int iter = 0;
    	    		
        	    	double ellipse_inner_calc = 0;
        	    	double ellipse_outer_calc = 0;
        	    	
        	    	double innerWidth = (thymusWidth/2) * ((centralExclusionFraction + curShell*(1-centralExclusionFraction)/nShells) + shellSpacingFactor );
        	    	double outerWidth = (thymusWidth/2) * ((centralExclusionFraction + (curShell+1)*(1-centralExclusionFraction)/nShells) - shellSpacingFactor );
        	    	
        	    	double innerHeight = (thymusHeight/2) * ((centralExclusionFraction + curShell*(1-centralExclusionFraction)/nShells) + shellSpacingFactor );
        	    	double outerHeight = (thymusHeight/2) * ((centralExclusionFraction + (curShell+1)*(1-centralExclusionFraction)/nShells) - shellSpacingFactor );
        	    	
        	    	double angleMin = Math.PI + angleStep*curShellSub + angleFactor*angleStep;
        	    	double angleMax = Math.PI + angleStep*curShellSub + angleStep - angleFactor*angleStep;
        	    	
        	    	double randomRadiusX;
        	    	double randomRadiusY;
        	    	double randomAngle;
        	    	
        	    	// Generate a random position within thymus shell and subdivision
        	    	do 
        	    	{
        	    		randomRadiusX = fixed_seed_rand.nextDouble() * ( outerWidth - innerWidth ) + innerWidth;
        	    		randomRadiusY = fixed_seed_rand.nextDouble() * ( outerHeight - innerHeight ) + innerHeight;
        	    		randomAngle = fixed_seed_rand.nextDouble() * ( angleMax - angleMin ) + angleMin;
        	    		
        				x = thymusCenterX + randomRadiusX * Math.cos(randomAngle);
        				y = thymusCenterY + randomRadiusY * Math.sin(randomAngle);
        				z = thymusCenterZ + (fixed_seed_rand.nextDouble()-0.5) * thymusLength;
        				        				
        				ellipse_inner_calc = Math.pow((x-thymusCenterX)/(innerWidth), 2) 
        						             + Math.pow((y-thymusCenterY)/(innerHeight), 2);
        				
        				ellipse_outer_calc = Math.pow((x-thymusCenterX)/(outerWidth), 2) 
        			                         + Math.pow((y-thymusCenterY)/(outerHeight), 2);
        				        				
        				iter += 1;
        	    	} while ( ( ( ellipse_inner_calc <= 1 ) || ( ellipse_outer_calc > 1 ) ) && iter < iterLim );
    	    		
					if (iter < iterLim) 
					{ // assume that position is invalid if greater/equal iteration limit
						Point3d newPos = new Point3d(x, y, z);
		    	    	
		    			UniversalCell cell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
		    			// Assign the biomechanical model and set dimensions
		    			TCellMedakaCenterBased3DModel mechModel = ((TCellMedakaCenterBased3DModel) cell.getEpisimBioMechanicalModelObject());		
		    			mechModel.setCellWidth(sizeTEC);
		    			mechModel.setCellHeight(sizeTEC);
		    			mechModel.setCellLength(sizeTEC);
		    			mechModel.setStandardCellWidth(CELL_WIDTH);
		    			mechModel.setStandardCellHeight(CELL_HEIGHT);
		    			mechModel.setStandardCellLength(CELL_LENGTH);
		    						
		    			// Add the new cell to the standard cell ensemble
		    			existingCoordinates.add(newPos);
		    			Double3D cellLocation = new Double3D(newPos.x, newPos.y, newPos.z);
		    			mechModel.setCellLocationInCellField(cellLocation);			
		    			
		    			EpisimModelConnector mc = mechModel.getEpisimModelConnector();
		    			if(mc != null && mc instanceof EpisimTCellMedakaCenterBased3DMC){
		    				EpisimTCellMedakaCenterBased3DMC modelConnector = (EpisimTCellMedakaCenterBased3DMC) mc;
		    				modelConnector.setTec_identity(id);
		    			}
		    			standardCellEnsemble.add(cell);
		    			
		    			// Define initial differentiation levels
		    			// 0 = default; 1 = TEC soma; 2 = TEC arm; 3 = TSP source; 4 = TSP; 5 = ab; 6 = gd; 7 = ab mature; 8 = gd mature
		    			EpisimDifferentiationLevel[] diffLevels = ModelController.getInstance().getCellBehavioralModelController().getAvailableDifferentiationLevels();
		    			cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[1]);
		    			
		    			// Instantiate arms for this cell
		    			int nArms = mechModelGP.getTec_Arms();
		    			int armElements = mechModelGP.getTecletsPerTEC();
		    			double distanceFactor = mechModelGP.getTecletDistanceFactor();
		    			
		    			double goldenRatio = (1 + Math.sqrt(5))/2;
		    			double tecletSize = sizeTEC/goldenRatio;
		    			double angleIncrement = 2*Math.PI/nArms;
		    			double angle = curShell%2 == 0? randomAngle : randomAngle + Math.PI/4;
		    			double cumulativeSize = 0;
		    			double angleWobble = mechModelGP.getTeclet_angleFactor();
	
		    			for ( int j = 0; j < nArms; j++ )
		    			{
							angle = angle + j*angleIncrement + (Math.PI*angleWobble*(fixed_seed_rand.nextDouble()-0.5));
							
		    				for ( int k = 1; k <= armElements; k++ )
		    				{
		    					double teclet_x = newPos.x + distanceFactor*(0.5*sizeTEC + 0.5*tecletSize + cumulativeSize)*Math.cos(angle);
		    					double teclet_y = newPos.y + distanceFactor*(0.5*sizeTEC + 0.5*tecletSize + cumulativeSize)*Math.sin(angle);
		    					double teclet_z = newPos.z;
		    					cumulativeSize += distanceFactor*tecletSize;
		    					
		    					// Check if cell is out of bounds
		    					
		    					if (!(teclet_x >= domainWidth*(1-domainBorderFraction) || teclet_y >= domainHeight*(1-domainBorderFraction)))
		    					{
			    	    			Point3d tecletPos = new Point3d(teclet_x, teclet_y, teclet_z);
			    					
			    					UniversalCell teclet = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
			    	    			// Assign the biomechanical model and set dimensions
			    	    			TCellMedakaCenterBased3DModel mechModel_teclet = ((TCellMedakaCenterBased3DModel) teclet.getEpisimBioMechanicalModelObject());		
			    	    			mechModel_teclet.setCellWidth(tecletSize);
			    	    			mechModel_teclet.setCellHeight(tecletSize);
			    	    			mechModel_teclet.setCellLength(tecletSize);
			    	    			mechModel_teclet.setStandardCellWidth(CELL_WIDTH);
			    	    			mechModel_teclet.setStandardCellHeight(CELL_HEIGHT);
			    	    			mechModel_teclet.setStandardCellLength(CELL_LENGTH);
			    	    						
			    	    			// Add the new cell to the standard cell ensemble
			    	    			existingCoordinates.add(tecletPos);
			    	    			Double3D tecletLocation = new Double3D(teclet_x, teclet_y, teclet_z);
			    	    			mechModel_teclet.setCellLocationInCellField(tecletLocation);			
			    	    			
			    	    			EpisimModelConnector teclet_mc = mechModel_teclet.getEpisimModelConnector();
			    	    			if(teclet_mc != null && teclet_mc instanceof EpisimTCellMedakaCenterBased3DMC){
			    	    				EpisimTCellMedakaCenterBased3DMC tecletModelConnector = (EpisimTCellMedakaCenterBased3DMC) teclet_mc;
			    	    				tecletModelConnector.setTec_identity(id);
			    	    			}
			    	    			standardCellEnsemble.add(teclet);
			    	    			
			    	    			// Define initial differentiation levels
			    	    			// 0 = default; 1 = TEC soma; 2 = TEC arm; 3 = TSP source; 4 = TSP; 5 = ab; 6 = gd; 7 = ab mature; 8 = gd mature
			    	    			teclet.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[2]);
			    	    			
			    	    			tecletSize = tecletSize/goldenRatio < mechModelGP.getMinimumTecletSize() ? mechModelGP.getMinimumTecletSize() : tecletSize/goldenRatio;
		    					}
		    				}
		    				
		    				// Reset variables
		    				tecletSize = sizeTEC/goldenRatio;
		    				angle = curShell%2 == 0? randomAngle : randomAngle + Math.PI/4;
		    				cumulativeSize = 0;
		    			}
					}
    	    	}
    		}
     		
     		int numInitTSPsource = mechModelGP.getNumberInitialTSPsource();
     		
    		double xMin = domainWidth*domainBorderFraction;
    		double xMax = domainWidth*(1-domainBorderFraction);
    		
    		double yMin = domainHeight*domainBorderFraction;
    		double yMax = domainHeight*(1-domainBorderFraction);
    		
     		for ( int i = 0; i < numInitTSPsource; i++ )
    	    {	
    	    	if (mammalTypeSource) // Mammal type source: between cortex and medulla
    	    	{
        	    	double ellipse_inner_calc = 0;
        	    	double ellipse_outer_calc = 0;
        	    	
        	    	do {
        				x = random_uniform.nextDouble() * ( xMax - xMin ) + xMin;
        				y = random_uniform.nextDouble() * ( yMax - yMin ) + yMin;
        				z = thymusCenterZ + (random_uniform.nextDouble()-0.5) * thymusLength;
        				
        				double factor_inner = medullaFactor*(1-domainBorderFraction);
        				double factor_outer = medullaFactor*(1+domainBorderFraction);
        				
        				ellipse_inner_calc = Math.pow((x-thymusCenterX)/(factor_inner*thymusWidth/2), 2) 
        						             + Math.pow((y-thymusCenterY)/(factor_inner*thymusHeight/2), 2);
        				
        				ellipse_outer_calc = Math.pow((x-thymusCenterX)/(factor_outer*thymusWidth/2), 2) 
        			                         + Math.pow((y-thymusCenterY)/(factor_outer*thymusHeight/2), 2);
        				
        	    	} while ( ( ellipse_inner_calc <= 1 ) || ( ellipse_outer_calc > 1 || y > thymusCenterY ) );
    	    	}
    	    	else // Fish type source: within migration cone beneath cortex
    	    	{	
    	    		double mod = i%2;
    	    		xMin = mod < 1 ? domainWidth*domainBorderFraction : domainWidth*(1-domainBorderFraction) - mechModelGP.getFishTSPsourceDomainSize();
    	    		xMax = mod < 1 ? domainWidth*domainBorderFraction + mechModelGP.getFishTSPsourceDomainSize() : domainWidth*(1-domainBorderFraction);
    	    		
        	    	do {
        				x = fixed_seed_rand.nextDouble() * ( xMax - xMin ) + xMin;
        				y = fixed_seed_rand.nextDouble() * ( yMax - yMin ) + yMin;
        				z = thymusCenterZ + (fixed_seed_rand.nextDouble()-0.5) * thymusLength;
        				
        	    	} while ( y > 2*domainHeight*domainBorderFraction );
    	    	}

    			Point3d newPos = new Point3d(x, y, z);
    	    	
    			UniversalCell cell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
    			// Assign the biomechanical model and set dimensions
    			TCellMedakaCenterBased3DModel mechModel = ((TCellMedakaCenterBased3DModel) cell.getEpisimBioMechanicalModelObject());		
    			mechModel.setCellWidth(sizeTSP_source);
    			mechModel.setCellHeight(sizeTSP_source);
    			mechModel.setCellLength(sizeTSP_source);
    			mechModel.setStandardCellWidth(CELL_WIDTH);
    			mechModel.setStandardCellHeight(CELL_HEIGHT);
    			mechModel.setStandardCellLength(CELL_LENGTH);
    						
    			// Add the new cell to the standard cell ensemble
    			existingCoordinates.add(newPos);
    			Double3D cellLocation = new Double3D(newPos.x, newPos.y, newPos.z);
    			mechModel.setCellLocationInCellField(cellLocation);			
    			
    			EpisimModelConnector mc = mechModel.getEpisimModelConnector();
    			if(mc != null && mc instanceof EpisimTCellMedakaCenterBased3DMC){
    				EpisimTCellMedakaCenterBased3DMC modelConnector = (EpisimTCellMedakaCenterBased3DMC) mc;
    			}
    			standardCellEnsemble.add(cell);
    			
    			// Define initial differentiation levels
    			// 0 = default; 1 = TEC soma; 2 = TEC arm; 3 = TSP source; 4 = TSP; 5 = ab; 6 = gd; 7 = ab mature; 8 = gd mature
    			EpisimDifferentiationLevel[] diffLevels = ModelController.getInstance().getCellBehavioralModelController().getAvailableDifferentiationLevels();
    			cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[3]);
    		}
     		
    		int numInitTSP = mechModelGP.getNumberInitialTSP();
    	    for ( int i = 0; i < numInitTSP; i++ )
    	    {
    	    	if (mammalTypeSource) // Mammal type source: between cortex and medulla
    	    	{
        	    	double ellipse_inner_calc = 0;
        	    	double ellipse_outer_calc = 0;
        	    	
        	    	do {
        				x = random_uniform.nextDouble() * (domainWidth*(1-2*domainBorderFraction)) + domainWidth*domainBorderFraction;
        				y = random_uniform.nextDouble() * (domainHeight*(1-2*domainBorderFraction)) + domainHeight*domainBorderFraction;
        				z = thymusCenterZ + (random_uniform.nextDouble()-0.5) * thymusLength;
        				
        				double factor_inner = medullaFactor*(1-domainBorderFraction);
        				double factor_outer = medullaFactor*(1+domainBorderFraction);
        				
        				ellipse_inner_calc = Math.pow((x-thymusCenterX)/(factor_inner*thymusWidth/2), 2) 
        						             + Math.pow((y-thymusCenterY)/(factor_inner*thymusHeight/2), 2);
        				
        				ellipse_outer_calc = Math.pow((x-thymusCenterX)/(factor_outer*thymusWidth/2), 2) 
        			                         + Math.pow((y-thymusCenterY)/(factor_outer*thymusHeight/2), 2);
        				
        	    	} while ( ( ellipse_inner_calc <= 1 ) || ( ellipse_outer_calc > 1 || y > thymusCenterY ) );
    	    	}
    	    	else // Fish type source: within entire thymus
    	    	{
        	    	double ellipse_calc = 0;
        	    	
        	    	do {
        				x = random_uniform.nextDouble() * (domainWidth*(1-2*domainBorderFraction)) + domainWidth*domainBorderFraction;
        				y = random_uniform.nextDouble() * (domainHeight*(1-2*domainBorderFraction)) + domainHeight*domainBorderFraction;
        				z = thymusCenterZ + (random_uniform.nextDouble()-0.5) * thymusLength;
        				
        				ellipse_calc = Math.pow((x-thymusCenterX)/(thymusWidth/2), 2) 
        			                         + Math.pow((y-thymusCenterY)/(thymusHeight/2), 2);
        				
        	    	} while ( ellipse_calc > 1 || y > thymusCenterY );
    	    	}

    			Point3d newPos = new Point3d(x, y, z);
    	    	
    			UniversalCell cell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
    			// Assign the biomechanical model and set dimensions
    			TCellMedakaCenterBased3DModel mechModel = ((TCellMedakaCenterBased3DModel) cell.getEpisimBioMechanicalModelObject());		
    			mechModel.setCellWidth(sizeTSP);
    			mechModel.setCellHeight(sizeTSP);
    			mechModel.setCellLength(sizeTSP);
    			mechModel.setStandardCellWidth(CELL_WIDTH);
    			mechModel.setStandardCellHeight(CELL_HEIGHT);
    			mechModel.setStandardCellLength(CELL_LENGTH);
    						
    			// Add the new cell to the standard cell ensemble
    			existingCoordinates.add(newPos);
    			Double3D cellLocation = new Double3D(newPos.x, newPos.y, newPos.z);
    			mechModel.setCellLocationInCellField(cellLocation);			
    			
    			EpisimModelConnector mc = mechModel.getEpisimModelConnector();
    			if(mc != null && mc instanceof EpisimTCellMedakaCenterBased3DMC){
    				EpisimTCellMedakaCenterBased3DMC modelConnector = (EpisimTCellMedakaCenterBased3DMC) mc;
    			}
    			standardCellEnsemble.add(cell);
    			
    			// Define initial differentiation levels
    			// 0 = default; 1 = TEC soma; 2 = TEC arm; 3 = TSP source; 4 = TSP; 5 = ab; 6 = gd; 7 = ab mature; 8 = gd mature
    			EpisimDifferentiationLevel[] diffLevels = ModelController.getInstance().getCellBehavioralModelController().getAvailableDifferentiationLevels();
    			cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[4]);
    		}
     		
     		int numInitAB = mechModelGP.getNumberInitialAB();
     		for ( int i = 0; i < numInitAB; i++ )
    	    {			
    	    	// Generate a random position within thymus medulla
    	    	double ellipse_calc = 0;
    	    	
    	    	do {
    				x = random_uniform.nextDouble() * (domainWidth*(1-2*domainBorderFraction)) + domainWidth*domainBorderFraction;
    				y = random_uniform.nextDouble() * (domainHeight*(1-2*domainBorderFraction)) + domainHeight*domainBorderFraction;
    				z = thymusCenterZ + (random_uniform.nextDouble()-0.5) * thymusLength;
    				
    				ellipse_calc = Math.pow((x-thymusCenterX)/(medullaFactor*thymusWidth/2), 2) 
    						       + Math.pow((y-thymusCenterY)/(medullaFactor*thymusHeight/2), 2);	
    	    	} while ( ( ellipse_calc > 1 || y > thymusCenterY ) );

    			Point3d newPos = new Point3d(x, y, z);
    	    	
    			UniversalCell cell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
    			// Assign the biomechanical model and set dimensions
    			TCellMedakaCenterBased3DModel mechModel = ((TCellMedakaCenterBased3DModel) cell.getEpisimBioMechanicalModelObject());		
    			mechModel.setCellWidth(sizeTSP);
    			mechModel.setCellHeight(sizeTSP);
    			mechModel.setCellLength(sizeTSP);
    			mechModel.setStandardCellWidth(CELL_WIDTH);
    			mechModel.setStandardCellHeight(CELL_HEIGHT);
    			mechModel.setStandardCellLength(CELL_LENGTH);
    						
    			// Add the new cell to the standard cell ensemble
    			existingCoordinates.add(newPos);
    			Double3D cellLocation = new Double3D(newPos.x, newPos.y, newPos.z);
    			mechModel.setCellLocationInCellField(cellLocation);			
    			
    			EpisimModelConnector mc = mechModel.getEpisimModelConnector();
    			if(mc != null && mc instanceof EpisimTCellMedakaCenterBased3DMC){
    				EpisimTCellMedakaCenterBased3DMC modelConnector = (EpisimTCellMedakaCenterBased3DMC) mc;
    			}
    			standardCellEnsemble.add(cell);
    			
    			// Define initial differentiation levels
    			// 0 = default; 1 = TEC soma; 2 = TEC arm; 3 = TSP source; 4 = TSP; 5 = ab; 6 = gd; 7 = ab mature; 8 = gd mature
    			double random_number = random_uniform.nextDouble();
    			double fraction_mature = mechModelGP.getFractionInitGD_mature();
    			EpisimDifferentiationLevel[] diffLevels = ModelController.getInstance().getCellBehavioralModelController().getAvailableDifferentiationLevels();
    			if ( random_number >= fraction_mature) 
    			{
    				cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[5]);
    			}
    			else
    			{
    				cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[7]);
    			}
    		}
     		
     		int numInitGD = mechModelGP.getNumberInitialGD();
     		for ( int i = 0; i < numInitGD; i++ )
    	    {			
    	    	// Generate a random position within thymus cortex
    	    	double ellipse_inner_calc = 0;
    	    	double ellipse_outer_calc = 0;
    	    	
    	    	do {
    				x = random_uniform.nextDouble() * (domainWidth*(1-2*domainBorderFraction)) + domainWidth*domainBorderFraction;
    				y = random_uniform.nextDouble() * (domainHeight*(1-2*domainBorderFraction)) + domainHeight*domainBorderFraction;
    				z = thymusCenterZ + (random_uniform.nextDouble()-0.5) * thymusLength;
    				
    				ellipse_inner_calc = Math.pow((x-thymusCenterX)/(medullaFactor*thymusWidth/2), 2) 
    						             + Math.pow((y-thymusCenterY)/(medullaFactor*thymusHeight/2), 2);
    				
    				ellipse_outer_calc = Math.pow((x-thymusCenterX)/(thymusWidth/2), 2) 
    			                         + Math.pow((y-thymusCenterY)/(thymusHeight/2), 2);
    				
    	    	} while ( ( ellipse_inner_calc <= 1 ) || ( ellipse_outer_calc > 1 || y > thymusCenterY ) );

    			Point3d newPos = new Point3d(x, y, z);
    	    	
    			UniversalCell cell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
    			// Assign the biomechanical model and set dimensions
    			TCellMedakaCenterBased3DModel mechModel = ((TCellMedakaCenterBased3DModel) cell.getEpisimBioMechanicalModelObject());		
    			mechModel.setCellWidth(sizeTSP);
    			mechModel.setCellHeight(sizeTSP);
    			mechModel.setCellLength(sizeTSP);
    			mechModel.setStandardCellWidth(CELL_WIDTH);
    			mechModel.setStandardCellHeight(CELL_HEIGHT);
    			mechModel.setStandardCellLength(CELL_LENGTH);
    						
    			// Add the new cell to the standard cell ensemble
    			existingCoordinates.add(newPos);
    			Double3D cellLocation = new Double3D(newPos.x, newPos.y, newPos.z);
    			mechModel.setCellLocationInCellField(cellLocation);			
    			
    			EpisimModelConnector mc = mechModel.getEpisimModelConnector();
    			if(mc != null && mc instanceof EpisimTCellMedakaCenterBased3DMC){
    				EpisimTCellMedakaCenterBased3DMC modelConnector = (EpisimTCellMedakaCenterBased3DMC) mc;
    			}
    			standardCellEnsemble.add(cell);
    			
    			// Define initial differentiation levels
    			// 0 = default; 1 = TEC soma; 2 = TEC arm; 3 = TSP source; 4 = TSP; 5 = ab; 6 = gd; 7 = ab mature; 8 = gd mature
    			double random_number = random_uniform.nextDouble();
    			double fraction_mature = mechModelGP.getFractionInitGD_mature();
    			EpisimDifferentiationLevel[] diffLevels = ModelController.getInstance().getCellBehavioralModelController().getAvailableDifferentiationLevels();
    			if ( random_number >= fraction_mature) 
    			{
    				cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[6]);
    			}
    			else
    			{
    				cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[8]);				
    			}
    		}
    	}
    	else { // full 3D ellipsoid model
    		int numInitTEC = 40; //mechModelGP.getNumberInitialTEC();
     		for ( int i = 0; i < numInitTEC; i++ )
    	    {			
    	    	// Generate a random position within thymus
    	    	double ellipsoid_calc = 0;
    	    	
    	    	do {
    				x = fixed_seed_rand.nextDouble() * (domainWidth*(1-2*domainBorderFraction)) + domainWidth*domainBorderFraction;
    				y = fixed_seed_rand.nextDouble() * (domainHeight*(1-2*domainBorderFraction)) + domainHeight*domainBorderFraction;
    				z = fixed_seed_rand.nextDouble() * (domainLength*(1-2*domainBorderFraction)) + domainLength*domainBorderFraction;
    				
    				ellipsoid_calc = Math.pow((x-thymusCenterX)/(thymusWidth/2), 2) 
    						       + Math.pow((y-thymusCenterY)/(thymusHeight/2), 2)
    						       + Math.pow((z-thymusCenterZ)/(thymusLength/2), 2);		
    	    	} while ( ellipsoid_calc > 1 );

    			Point3d newPos = new Point3d(x, y, z);
    	    	
    			UniversalCell cell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
    			// Assign the biomechanical model and set dimensions
    			TCellMedakaCenterBased3DModel mechModel = ((TCellMedakaCenterBased3DModel) cell.getEpisimBioMechanicalModelObject());		
    			mechModel.setCellWidth(sizeTEC);
    			mechModel.setCellHeight(sizeTEC);
    			mechModel.setCellLength(sizeTEC);
    			mechModel.setStandardCellWidth(CELL_WIDTH);
    			mechModel.setStandardCellHeight(CELL_HEIGHT);
    			mechModel.setStandardCellLength(CELL_LENGTH);
    						
    			// Add the new cell to the standard cell ensemble
    			existingCoordinates.add(newPos);
    			Double3D cellLocation = new Double3D(newPos.x, newPos.y, newPos.z);
    			mechModel.setCellLocationInCellField(cellLocation);			
    			
    			EpisimModelConnector mc = mechModel.getEpisimModelConnector();
    			if(mc != null && mc instanceof EpisimTCellMedakaCenterBased3DMC){
    				EpisimTCellMedakaCenterBased3DMC modelConnector = (EpisimTCellMedakaCenterBased3DMC) mc;
    			}
    			standardCellEnsemble.add(cell);
    			
    			// Define initial differentiation levels
    			// 0 = default; 1 = TEC; 2 = TSP source; 3 = TSP; 4 = ab; 5 = gd
    			EpisimDifferentiationLevel[] diffLevels = ModelController.getInstance().getCellBehavioralModelController().getAvailableDifferentiationLevels();
    			cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[1]);
    		}
     		
     		int numInitTSPsource = mechModelGP.getNumberInitialTSPsource();
     		for ( int i = 0; i < numInitTSPsource; i++ )
    	    {			
    	    	// Generate a random position within migration cone	base    	
    	    	double cone_calc = 0;
    	    	
    	    	do {
    				x = fixed_seed_rand.nextDouble() * (domainWidth*(1-2*domainBorderFraction)) + domainWidth*domainBorderFraction;
    				y = fixed_seed_rand.nextDouble() * (domainHeight*(1-2*domainBorderFraction)) + domainHeight*domainBorderFraction;
    				z = fixed_seed_rand.nextDouble() * (domainLength*(1-2*domainBorderFraction)) + domainLength*domainBorderFraction;
    				
    				cone_calc = Math.pow((x-thymusCenterX)/(thymusWidth), 2) 
    					      + Math.pow((z-thymusCenterZ)/(thymusLength), 2)
    					      - Math.pow((y-thymusCenterY)/migrationConeHeight, 2);				
    	    	} while ( cone_calc > 0 || y > 2*domainHeight*domainBorderFraction );

    			Point3d newPos = new Point3d(x, y, z);
    	    	
    			UniversalCell cell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
    			// Assign the biomechanical model and set dimensions
    			TCellMedakaCenterBased3DModel mechModel = ((TCellMedakaCenterBased3DModel) cell.getEpisimBioMechanicalModelObject());		
    			mechModel.setCellWidth(sizeTSP_source);
    			mechModel.setCellHeight(sizeTSP_source);
    			mechModel.setCellLength(sizeTSP_source);
    			mechModel.setStandardCellWidth(CELL_WIDTH);
    			mechModel.setStandardCellHeight(CELL_HEIGHT);
    			mechModel.setStandardCellLength(CELL_LENGTH);
    						
    			// Add the new cell to the standard cell ensemble
    			existingCoordinates.add(newPos);
    			Double3D cellLocation = new Double3D(newPos.x, newPos.y, newPos.z);
    			mechModel.setCellLocationInCellField(cellLocation);			
    			
    			EpisimModelConnector mc = mechModel.getEpisimModelConnector();
    			if(mc != null && mc instanceof EpisimTCellMedakaCenterBased3DMC){
    				EpisimTCellMedakaCenterBased3DMC modelConnector = (EpisimTCellMedakaCenterBased3DMC) mc;
    			}
    			standardCellEnsemble.add(cell);
    			
    			// Define initial differentiation levels
    			// 0 = default; 1 = TEC; 2 = TSP source; 3 = TSP; 4 = ab; 5 = gd
    			EpisimDifferentiationLevel[] diffLevels = ModelController.getInstance().getCellBehavioralModelController().getAvailableDifferentiationLevels();
    			cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[2]);
    		}
     		
    		int numInitTSP = mechModelGP.getNumberInitialTSP();
    	    for ( int i = 0; i < numInitTSP; i++ )
    	    {			
    	    	// Generate a random position within migration cone + thymus
    	    	double ellipsoid_calc = 0;
    	    	double cone_calc = 0;
    	    	
    	    	do {
    				x = fixed_seed_rand.nextDouble() * (domainWidth*(1-2*domainBorderFraction)) + domainWidth*domainBorderFraction;
    				y = fixed_seed_rand.nextDouble() * (domainHeight*(1-2*domainBorderFraction)) + domainHeight*domainBorderFraction;
    				z = fixed_seed_rand.nextDouble() * (domainLength*(1-2*domainBorderFraction)) + domainLength*domainBorderFraction;
    				
    				ellipsoid_calc = Math.pow((x-thymusCenterX)/(thymusWidth/2), 2) 
    						       + Math.pow((y-thymusCenterY)/(thymusHeight/2), 2)
    						       + Math.pow((z-thymusCenterZ)/(thymusLength/2), 2);
    				
    				cone_calc = Math.pow((x-thymusCenterX)/(thymusWidth), 2) 
    					      + Math.pow((z-thymusCenterZ)/(thymusLength), 2)
    					      - Math.pow((y-thymusCenterY)/migrationConeHeight, 2);				
    	    	} while ( ellipsoid_calc > 1 && (cone_calc > 0 || y > thymusCenterY));

    			Point3d newPos = new Point3d(x, y, z);
    	    	
    			UniversalCell cell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
    			// Assign the biomechanical model and set dimensions
    			TCellMedakaCenterBased3DModel mechModel = ((TCellMedakaCenterBased3DModel) cell.getEpisimBioMechanicalModelObject());		
    			mechModel.setCellWidth(sizeTSP);
    			mechModel.setCellHeight(sizeTSP);
    			mechModel.setCellLength(sizeTSP);
    			mechModel.setStandardCellWidth(CELL_WIDTH);
    			mechModel.setStandardCellHeight(CELL_HEIGHT);
    			mechModel.setStandardCellLength(CELL_LENGTH);
    						
    			// Add the new cell to the standard cell ensemble
    			existingCoordinates.add(newPos);
    			Double3D cellLocation = new Double3D(newPos.x, newPos.y, newPos.z);
    			mechModel.setCellLocationInCellField(cellLocation);			
    			
    			EpisimModelConnector mc = mechModel.getEpisimModelConnector();
    			if(mc != null && mc instanceof EpisimTCellMedakaCenterBased3DMC){
    				EpisimTCellMedakaCenterBased3DMC modelConnector = (EpisimTCellMedakaCenterBased3DMC) mc;
    			}
    			standardCellEnsemble.add(cell);
    			
    			// Define initial differentiation levels
    			// 0 = default; 1 = TEC; 2 = TSP source; 3 = TSP; 4 = ab; 5 = gd
    			EpisimDifferentiationLevel[] diffLevels = ModelController.getInstance().getCellBehavioralModelController().getAvailableDifferentiationLevels();
    			cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[3]);
    		}
     		
     		int numInitAB = mechModelGP.getNumberInitialAB();
     		for ( int i = 0; i < numInitAB; i++ )
    	    {			
    	    	// Generate a random position within thymus medulla
    	    	double ellipsoid_calc = 0;
    	    	
    	    	do {
    				x = fixed_seed_rand.nextDouble() * (domainWidth*(1-2*domainBorderFraction)) + domainWidth*domainBorderFraction;
    				y = fixed_seed_rand.nextDouble() * (domainHeight*(1-2*domainBorderFraction)) + domainHeight*domainBorderFraction;
    				z = fixed_seed_rand.nextDouble() * (domainLength*(1-2*domainBorderFraction)) + domainLength*domainBorderFraction;
    				
    				ellipsoid_calc = Math.pow((x-thymusCenterX)/(medullaFactor*thymusWidth/2), 2) 
    						       + Math.pow((y-thymusCenterY)/(medullaFactor*thymusHeight/2), 2)
    						       + Math.pow((z-thymusCenterZ)/(medullaFactor*thymusLength/2), 2);	
    	    	} while ( ellipsoid_calc > 1 );

    			Point3d newPos = new Point3d(x, y, z);
    	    	
    			UniversalCell cell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
    			// Assign the biomechanical model and set dimensions
    			TCellMedakaCenterBased3DModel mechModel = ((TCellMedakaCenterBased3DModel) cell.getEpisimBioMechanicalModelObject());		
    			mechModel.setCellWidth(sizeTSP);
    			mechModel.setCellHeight(sizeTSP);
    			mechModel.setCellLength(sizeTSP);
    			mechModel.setStandardCellWidth(CELL_WIDTH);
    			mechModel.setStandardCellHeight(CELL_HEIGHT);
    			mechModel.setStandardCellLength(CELL_LENGTH);
    						
    			// Add the new cell to the standard cell ensemble
    			existingCoordinates.add(newPos);
    			Double3D cellLocation = new Double3D(newPos.x, newPos.y, newPos.z);
    			mechModel.setCellLocationInCellField(cellLocation);			
    			
    			EpisimModelConnector mc = mechModel.getEpisimModelConnector();
    			if(mc != null && mc instanceof EpisimTCellMedakaCenterBased3DMC){
    				EpisimTCellMedakaCenterBased3DMC modelConnector = (EpisimTCellMedakaCenterBased3DMC) mc;
    			}
    			standardCellEnsemble.add(cell);
    			
    			// Define initial differentiation levels
    			// 0 = default; 1 = TEC soma; 2 = TEC arm; 3 = TSP source; 4 = TSP; 5 = ab; 6 = gd; 7 = ab mature; 8 = gd mature
    			double random_number = fixed_seed_rand.nextDouble();
    			double fraction_mature = mechModelGP.getFractionInitAB_mature();
    			EpisimDifferentiationLevel[] diffLevels = ModelController.getInstance().getCellBehavioralModelController().getAvailableDifferentiationLevels();
    			if ( random_number >= fraction_mature) 
    			{
    				cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[5]);
    			}
    			else
    			{
    				cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[7]);				
    			}
    		}
     		
     		int numInitGD = mechModelGP.getNumberInitialGD();
     		for ( int i = 0; i < numInitGD; i++ )
    	    {			
    	    	// Generate a random position within thymus cortex
    	    	double ellipsoid_inner_calc = 0;
    	    	double ellipsoid_outer_calc = 0;
    	    	
    	    	do {
    				x = fixed_seed_rand.nextDouble() * (domainWidth*(1-2*domainBorderFraction)) + domainWidth*domainBorderFraction;
    				y = fixed_seed_rand.nextDouble() * (domainHeight*(1-2*domainBorderFraction)) + domainHeight*domainBorderFraction;
    				z = fixed_seed_rand.nextDouble() * (domainLength*(1-2*domainBorderFraction)) + domainLength*domainBorderFraction;
    				
    				ellipsoid_inner_calc = Math.pow((x-thymusCenterX)/(medullaFactor*thymusWidth/2), 2) 
    						             + Math.pow((y-thymusCenterY)/(medullaFactor*thymusHeight/2), 2)
    						             + Math.pow((z-thymusCenterZ)/(medullaFactor*thymusLength/2), 2);
    				
    				ellipsoid_outer_calc = Math.pow((x-thymusCenterX)/(thymusWidth/2), 2) 
    			                         + Math.pow((y-thymusCenterY)/(thymusHeight/2), 2)
    			                         + Math.pow((z-thymusCenterZ)/(thymusLength/2), 2);
    				
    	    	} while ( ellipsoid_inner_calc <= 1 || ellipsoid_outer_calc > 1 );

    			Point3d newPos = new Point3d(x, y, z);
    	    	
    			UniversalCell cell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
    			// Assign the biomechanical model and set dimensions
    			TCellMedakaCenterBased3DModel mechModel = ((TCellMedakaCenterBased3DModel) cell.getEpisimBioMechanicalModelObject());		
    			mechModel.setCellWidth(sizeTSP);
    			mechModel.setCellHeight(sizeTSP);
    			mechModel.setCellLength(sizeTSP);
    			mechModel.setStandardCellWidth(CELL_WIDTH);
    			mechModel.setStandardCellHeight(CELL_HEIGHT);
    			mechModel.setStandardCellLength(CELL_LENGTH);
    						
    			// Add the new cell to the standard cell ensemble
    			existingCoordinates.add(newPos);
    			Double3D cellLocation = new Double3D(newPos.x, newPos.y, newPos.z);
    			mechModel.setCellLocationInCellField(cellLocation);			
    			
    			EpisimModelConnector mc = mechModel.getEpisimModelConnector();
    			if(mc != null && mc instanceof EpisimTCellMedakaCenterBased3DMC){
    				EpisimTCellMedakaCenterBased3DMC modelConnector = (EpisimTCellMedakaCenterBased3DMC) mc;
    			}
    			standardCellEnsemble.add(cell);
    			
    			// Define initial differentiation levels
    			// 0 = default; 1 = TEC soma; 2 = TEC arm; 3 = TSP source; 4 = TSP; 5 = ab; 6 = gd; 7 = ab mature; 8 = gd mature
    			double random_number = fixed_seed_rand.nextDouble();
    			double fraction_mature = mechModelGP.getFractionInitAB_mature();
    			EpisimDifferentiationLevel[] diffLevels = ModelController.getInstance().getCellBehavioralModelController().getAvailableDifferentiationLevels();
    			if ( random_number >= fraction_mature) 
    			{
    				cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[6]);
    			}
    			else
    			{
    				cell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[8]);				
    			}
    		}
    	}
 		
	    
	    /*
	     *  Once all the cells have been placed, calculate the force balance to distribute them properly.
	     *  During this initial relaxation, the cells gain a random movement that is gradually decreased at each iteration.
	     *  At the end the randomness parameter is reset to its original value.
	     */
	    double randomness = mechModelGP.getRandomness();
		initializeBiomechanics(standardCellEnsemble);
		mechModelGP.setRandomness(randomness);
		return standardCellEnsemble;
	}
	
	// This part is the initial mechanical relaxation of the model, before the proper simulation starts
	private void initializeBiomechanics(ArrayList<UniversalCell> standardCellEnsemble)
	{
		EpisimBiomechanicalModel biomech = ModelController.getInstance().getBioMechanicalModelController().getNewEpisimBioMechanicalModelObject(null, null);
		TCellMedakaCenterBased3DModelGP mechModelGP = (TCellMedakaCenterBased3DModelGP) ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters();
		
		if(biomech instanceof TCellMedakaCenterBased3DModel)
		{
			double itermax = 50;
			double i = 1;
			double randomness_initial = 0.25;
			double randomness = randomness_initial;
			mechModelGP.setRandomness(randomness_initial);
			TCellMedakaCenterBased3DModel cbBioMech = (TCellMedakaCenterBased3DModel) biomech;			
			double cumulativeMigrationDist = 0;
			do
			{
				cbBioMech.initialisationGlobalSimStep();
				cumulativeMigrationDist = getCumulativeMigrationDistance(standardCellEnsemble);
				randomness = randomness_initial*(1-(i/itermax)) + 0.01;
				mechModelGP.setRandomness(randomness);
				i++;
			}
			// Continue calculating until cells have reached a stable distribution (cumulative migration distance < threshold)
			while(standardCellEnsemble.size() > 0 && ((cumulativeMigrationDist / standardCellEnsemble.size()) > mechModelGP.getMinAverageMigrationMikron()) && i <= itermax);
		}
	}
	
	// Get migration of cells during relaxation
	private double getCumulativeMigrationDistance(ArrayList<UniversalCell> standardCellEnsemble)
	{
		double cumulativeMigrationDistance = 0;
		
		for(int i = 0; i < standardCellEnsemble.size(); i++)
		{
			TCellMedakaCenterBased3DModel mechModel = ((TCellMedakaCenterBased3DModel) standardCellEnsemble.get(i).getEpisimBioMechanicalModelObject());
			cumulativeMigrationDistance += mechModel.getMigrationDistPerSimStep();
		}
		return cumulativeMigrationDistance;
	}
	
 	/*
 	 *  This function is used when loading a simulation state from a snapshot.
 	 *  It does essentially the same as the function "buildStandardInitialCellEnsemble"
 	 *  but doesn't need to generate points and calculate an initial mechanical relaxation.
 	 */
	protected ArrayList<UniversalCell> buildInitialCellEnsemble()
	{
        ArrayList<UniversalCell> loadedCells = super.buildInitialCellEnsemble();
		EpisimCellBehavioralModelGlobalParameters cbGP = ModelController.getInstance().getEpisimCellBehavioralModelGlobalParameters();		
		
		try
		{
	        Field field = cbGP.getClass().getDeclaredField("WIDTH_DEFAULT");
	        CELL_WIDTH = field.getDouble(cbGP);
	        field = cbGP.getClass().getDeclaredField("HEIGHT_DEFAULT");
	        CELL_HEIGHT = field.getDouble(cbGP);
	        field = cbGP.getClass().getDeclaredField("LENGTH_DEFAULT");
	        CELL_LENGTH = field.getDouble(cbGP);   
        }
        catch (NoSuchFieldException e)
        {
            EpisimExceptionHandler.getInstance().displayException(e);
        }
        catch (SecurityException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }
        catch (IllegalArgumentException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }
        catch (IllegalAccessException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }			
		
		double cellSize = Math.max(CELL_WIDTH, CELL_HEIGHT);
		cellSize = Math.max(cellSize, CELL_LENGTH);
		
		return loadedCells;
	}

	protected void initializeCellEnsembleBasedOnRandomAgeDistribution(ArrayList<UniversalCell> cellEnsemble)
	{
		// This method has to be implemented but has nothing to do in this model
	}

	/*
	 * The portrayal functions instantiate the 3D representation of the cells.
	 */
	protected EpisimPortrayal getCellPortrayal() 
	{
		ContinuousCellFieldPortrayal3D continuousPortrayal = new ContinuousCellFieldPortrayal3D("T cells");
		continuousPortrayal.setField(ModelController.getInstance().getBioMechanicalModelController().getCellField());
		return continuousPortrayal;
	}

	protected EpisimPortrayal[] getAdditionalPortrayalsCellForeground() 
	{
		return new EpisimPortrayal[0];
	}

	protected EpisimPortrayal[] getAdditionalPortrayalsCellBackground() 
	{
		return new EpisimPortrayal[0];
	}
}