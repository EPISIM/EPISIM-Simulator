package episimmcc.centerbased3d.tcellmedaka.students;
 
/*
 * Model connector for fish eye biomechanical parameters
 */

import java.util.HashMap;

import sim.app.episim.model.AbstractCell;

import sim.app.episim.model.biomechanics.centerbased3d.tcellmedaka.students.TCellMedakaCenterBased3DModel;
import sim.app.episim.model.biomechanics.centerbased3d.tcellmedaka.students.TCellMedakaCenterBased3DModelGP;

import sim.app.episim.model.initialization.BiomechanicalModelInitializer;
import episiminterfaces.EpisimBiomechanicalModel;
import episiminterfaces.EpisimBiomechanicalModelGlobalParameters;
import episiminterfaces.NoExport;
import episimmcc.EpisimModelConnector;

public class EpisimTCellMedakaCenterBased3DMC extends EpisimModelConnector 
{
    private static final String ID = "2021-11-07";
	private static final String NAME = "T Cell Center Based Biomechanical 3D Model";
	
	private HashMap<Long, Double> contactArea = new HashMap<Long, Double>();
	private HashMap<Long, Double> cellcellAdhesion = new HashMap<Long, Double>();
	private double bmContactArea = 0;

	private double average_overlap = 0;
	
	private double cellVolume = 0;
	private double extCellSpaceVolume = 0;
	private double extCellSpaceMikron = 0.2d;
	private double cellSurfaceArea = 0;
	private double totalContactArea = 0;	
	
	private double x = 0;
	private double y = 0;
	private double z = 0;
	private double width = 0;
	private double height = 0;
	private double length = 0;
	private double biasX = 0;
	private double biasY = 0;
	private double biasZ = 0;
	
	private double walkX = 0;
	private double walkY = 0;
	private double walkZ = 0;
	
	private double thymusDistanceX = 0;
	private double thymusDistanceY = 0;
	private double thymusDistanceZ = 0;
	
	private double medullaDistanceX = 0;
	private double medullaDistanceY = 0;
	private double medullaDistanceZ = 0;
	
	private boolean cellInThymus = false;
	private boolean cellInMigrationCone = false;
	
	private boolean inMedulla = false;
	private boolean inCortex = false;
	
	private boolean phase_switch = false;
	private double phase_mean = 0;
	private int phase_steps = 0;
	private double phase_duration = 0;
	
	private int tec_identity = 0;
	
	// Values set to be equal to the corresponding parameter in the global parameters during simulation runtime
	private int numberOfSecondsPerSimStep = 30;

	private double thymusCenterX = 250;
	private double thymusCenterY = 125;
	private double thymusCenterZ = 125;
	
	private double domainWidth = 500;
	private double domainHeight = 250;
	private double domainLength = 250;
	
	private double df_resolution = 2;
	private int latticeCells = 0;
	
	private double adhesionMembrane=0;
	
	private int cellId = -1;
		
	public double getX() {	
		return x;
	}

	@Hidden
	public void setX(double x) {	
		this.x = x;
	}

	public double getY() {	
		return y;
	}

	@Hidden
	public void setY(double y) {	
		this.y = y;
	}
	
	public double getZ() {	
		return z;
	}

	@Hidden
	public void setZ(double z) {	
		this.z = z;
	}
	
	public double getWidth() {	
		return width;
	}
	
	public void setWidth(double width) {	
		this.width = width;
	}
	
	public double getHeight() {	
		return height;
	}
	
	public void setHeight(double height) {	
		this.height = height;
	}
	
	public double getLength() {	
		return length;
	}
	
	public void setLength(double length) {	
		this.length = length;
	}	
   
    public double getAdhesionMembrane() {      
   	    return adhesionMembrane;
    }	
   
    public void setAdhesionMembrane(double adhesionMembrane) {   
   	    this.adhesionMembrane = adhesionMembrane;
    }
   
    @NoExport
	public void resetPairwiseParameters(){
		//this.contactArea.clear();
	}
	
	@Hidden
	@NoExport
	protected String getIdForInternalUse(){
		return ID;
	}
	
	@Hidden
	@NoExport
	public String getBiomechanicalModelName(){
		return NAME;
	}
	
	@NoExport
	public Class<? extends EpisimBiomechanicalModel> getEpisimBioMechanicalModelClass(){
		return TCellMedakaCenterBased3DModel.class;
	}
	
	@NoExport
	public Class<? extends EpisimBiomechanicalModelGlobalParameters> getEpisimBioMechanicalModelGlobalParametersClass(){
		return TCellMedakaCenterBased3DModelGP.class;
	}
	
	@NoExport
	public Class<? extends BiomechanicalModelInitializer> getEpisimBioMechanicalModelInitializerClass(){
		return TCellMedakaCenterBasedMechModelInit.class;
	}	
   
    @Hidden
    @Pairwise
    @NoExport
    public void setContactArea(long cellId, double contactArea){
   	    this.contactArea.put(cellId, contactArea);
    }
   
    @Pairwise
    @NoExport
    public double getContactArea(long cellId){
   	    return this.contactArea.containsKey(cellId)? this.contactArea.get(cellId):0;
    }
   
    @Hidden
    public void setContactArea(HashMap<Long, Double> contactArea){
   	    if(contactArea != null) this.contactArea = contactArea;
    }
   
    @Hidden
    public HashMap<Long, Double> getContactArea(){
   	    return this.contactArea;
    }   
   
    @Pairwise
    @NoExport
    public void setCellCellAdhesion(long cellId, double adhesion){
   	    this.cellcellAdhesion.put(cellId, adhesion);
    }
   
    @Pairwise
    @NoExport
    public double getCellCellAdhesion(long cellId){
   	    return this.cellcellAdhesion.containsKey(cellId)? this.cellcellAdhesion.get(cellId):0;
    }
   
    @Hidden
    public void setCellCellAdhesion(HashMap<Long, Double> cellcellAdhesion){
   	    if(cellcellAdhesion != null) this.cellcellAdhesion = cellcellAdhesion;
    }
   
    @Hidden
    public HashMap<Long, Double> getCellCellAdhesion(){
   	    return this.cellcellAdhesion;
    }  
   
	@Hidden
	@NoExport
    public double getAdhesionFactorForCell(AbstractCell cell){   	 	
   	    return cell != null && this.cellcellAdhesion.containsKey(cell.getID()) ? this.cellcellAdhesion.get(cell.getID()).doubleValue():0;
    }
	
    public double getCellVolume() {   
   	    return cellVolume;
    }

    @Hidden
    public void setCellVolume(double cellVolume) {   
   	    this.cellVolume = cellVolume;
    }
	
    public double getExtCellSpaceVolume() {   
   	    return extCellSpaceVolume;
    }

    @Hidden
    public void setExtCellSpaceVolume(double extCellSpaceVolume) {   
   	    this.extCellSpaceVolume = extCellSpaceVolume;
    }
	
    public double getExtCellSpaceMikron() {   
   	    return extCellSpaceMikron;
    }
	
    public void setExtCellSpaceMikron(double extCellSpaceMikron) {   
   	    this.extCellSpaceMikron = extCellSpaceMikron;
    }
	
    public double getCellSurfaceArea() {   
   	    return cellSurfaceArea;
    }
   
    @Hidden
    public void setCellSurfaceArea(double cellSurfaceArea) {   
   	    this.cellSurfaceArea = cellSurfaceArea;
    }
	
    public double getBmContactArea() {      
   	    return bmContactArea;
    }

    @Hidden
    public void setBmContactArea(double bmContactArea) {   
   	    this.bmContactArea = bmContactArea;
    }
   
    public double getTotalContactArea() {      
   	    return totalContactArea;
    }

    @Hidden
    public void setTotalContactArea(double totalContactArea) {   
   	    this.totalContactArea = totalContactArea;
    }

    public double getAverage_overlap() {
	   return average_overlap;
    }
	
	@Hidden
	public void setAverage_overlap(double average_overlap) {
	   this.average_overlap = average_overlap;
    }
	
    public int getCellId() { 
   	    return cellId;
    }

    @Hidden
    public void setCellId(int cellId) {   
   	    this.cellId = cellId;
    }

    public double getBiasX() {   
   	    return biasX;
    }

    public void setBiasX(double biasX) {   
   	    this.biasX = biasX;
    }

    public double getBiasY() {   
   	    return biasY;
    }

    public void setBiasY(double biasY) {   
   	    this.biasY = biasY;
    }

    public double getBiasZ() {   
   	    return biasZ;
    }

    public void setBiasZ(double biasZ) {   
   	    this.biasZ = biasZ;
    }

	public double getPhase_mean() {
		return phase_mean;
	}

	public void setPhase_mean(double phase_mean) {
		this.phase_mean = phase_mean;
	}

	public int getPhase_steps() {
		return phase_steps;
	}

	public void setPhase_steps(int phase_steps) {
		this.phase_steps = phase_steps;
	}

	public double getPhase_duration() {
		return phase_duration;
	}

	public void setPhase_duration(double phase_duration) {
		this.phase_duration = phase_duration;
	}

	public boolean getPhase_switch() {
		return phase_switch;
	}

	public void setPhase_switch(boolean phase_switch) {
		this.phase_switch = phase_switch;
	}

	public int getNumberOfSecondsPerSimStep() {
		return numberOfSecondsPerSimStep;
	}

	@Hidden
	public void setNumberOfSecondsPerSimStep(int numberOfSecondsPerSimStep) {
		this.numberOfSecondsPerSimStep = numberOfSecondsPerSimStep;
	}

	public double getWalkX() {
		return walkX;
	}

	public void setWalkX(double walkX) {
		this.walkX = walkX;
	}

	public double getWalkY() {
		return walkY;
	}

	public void setWalkY(double walkY) {
		this.walkY = walkY;
	}

	public double getWalkZ() {
		return walkZ;
	}

	public void setWalkZ(double walkZ) {
		this.walkZ = walkZ;
	}

	public double getThymusDistanceX() {
		return thymusDistanceX;
	}

	@Hidden
	public void setThymusDistanceX(double thymusDistanceX) {
		this.thymusDistanceX = thymusDistanceX;
	}

	public double getThymusDistanceY() {
		return thymusDistanceY;
	}

	@Hidden
	public void setThymusDistanceY(double thymusDistanceY) {
		this.thymusDistanceY = thymusDistanceY;
	}

	public double getThymusDistanceZ() {
		return thymusDistanceZ;
	}

	@Hidden
	public void setThymusDistanceZ(double thymusDistanceZ) {
		this.thymusDistanceZ = thymusDistanceZ;
	}

	public double getThymusCenterX() {
		return thymusCenterX;
	}

	@Hidden
	public void setThymusCenterX(double thymusCenterX) {
		this.thymusCenterX = thymusCenterX;
	}

	public double getThymusCenterY() {
		return thymusCenterY;
	}

	@Hidden
	public void setThymusCenterY(double thymusCenterY) {
		this.thymusCenterY = thymusCenterY;
	}

	public double getThymusCenterZ() {
		return thymusCenterZ;
	}

	@Hidden
	public void setThymusCenterZ(double thymusCenterZ) {
		this.thymusCenterZ = thymusCenterZ;
	}
	
	public boolean getCellInThymus() {
		return cellInThymus;
	}

	@Hidden
	public void setCellInThymus(boolean cellInThymus) {
		this.cellInThymus = cellInThymus;
	}

	public double getDomainWidth() {
		return domainWidth;
	}

	@Hidden
	public void setDomainWidth(double domainWidth) {
		this.domainWidth = domainWidth;
	}

	public double getDomainHeight() {
		return domainHeight;
	}

	@Hidden
	public void setDomainHeight(double domainHeight) {
		this.domainHeight = domainHeight;
	}

	public double getDomainLength() {
		return domainLength;
	}

	@Hidden
	public void setDomainLength(double domainLength) {
		this.domainLength = domainLength;
	}

	public boolean getCellInMigrationCone() {
		return cellInMigrationCone;
	}

	@Hidden
	public void setCellInMigrationCone(boolean cellInMigrationCone) {
		this.cellInMigrationCone = cellInMigrationCone;
	}

	public boolean getInMedulla() {
		return inMedulla;
	}

	@Hidden
	public void setInMedulla(boolean inMedulla) {
		this.inMedulla = inMedulla;
	}

	public boolean getInCortex() {
		return inCortex;
	}

	@Hidden
	public void setInCortex(boolean inCortex) {
		this.inCortex = inCortex;
	}

	public int getLatticeCells() {
		return latticeCells;
	}

	@Hidden
	public void setLatticeCells(int latticeCells) {
		this.latticeCells = latticeCells;
	}

	public double getDf_resolution() {
		return df_resolution;
	}

	public void setDf_resolution(double df_resolution) {
		this.df_resolution = df_resolution;
	}

	public double getMedullaDistanceX() {
		return medullaDistanceX;
	}

	public void setMedullaDistanceX(double medullaDistanceX) {
		this.medullaDistanceX = medullaDistanceX;
	}

	public double getMedullaDistanceY() {
		return medullaDistanceY;
	}

	public void setMedullaDistanceY(double medullaDistanceY) {
		this.medullaDistanceY = medullaDistanceY;
	}

	public double getMedullaDistanceZ() {
		return medullaDistanceZ;
	}

	public void setMedullaDistanceZ(double medullaDistanceZ) {
		this.medullaDistanceZ = medullaDistanceZ;
	}

	public int getTec_identity() {
		return tec_identity;
	}

	public void setTec_identity(int tec_identity) {
		this.tec_identity = tec_identity;
	}

}