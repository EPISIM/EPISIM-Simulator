package episimmcc.centerbased3d.fisheye;

/*
 * Sets model initial conditions
 * * Initial cell position (calculated with icosahedral mesh)
 * * Differentiation levels
 * * Initial parameter values
 */

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import ec.util.MersenneTwisterFast;
import sim.app.episim.EpisimExceptionHandler;
import sim.app.episim.model.UniversalCell;
import sim.app.episim.model.biomechanics.centerbased3d.fisheye.FishEyeCenterBased3DModel;
import sim.app.episim.model.biomechanics.centerbased3d.fisheye.FishEyeCenterBased3DModelGP;
import sim.app.episim.model.controller.ModelController;
import sim.app.episim.model.controller.TissueController;
import sim.app.episim.model.initialization.BiomechanicalModelInitializer;
import sim.app.episim.model.misc.MiscalleneousGlobalParameters;
import sim.app.episim.model.misc.MiscalleneousGlobalParameters.MiscalleneousGlobalParameters3D;
import sim.app.episim.persistence.SimulationStateData;
import sim.app.episim.util.Icosahedron;
import sim.app.episim.visualization.threedim.ContinuousCellFieldPortrayal3D;
import episiminterfaces.EpisimBiomechanicalModel;
import episiminterfaces.EpisimCellBehavioralModelGlobalParameters;
import episiminterfaces.EpisimDifferentiationLevel;
import episiminterfaces.EpisimPortrayal;
import episimmcc.EpisimModelConnector;
import episimmcc.centerbased3d.apicalmeristem.EpisimApicalMeristemCenterBased3DMC;


public class FishEyeCenterBasedMechModelInit extends BiomechanicalModelInitializer {

    // Fields
	SimulationStateData simulationStateData = null;
	private static double CELL_WIDTH = 0;
	private static double CELL_HEIGHT = 0;
	private static double CELL_LENGTH = 0;

	// Default constructor
	public FishEyeCenterBasedMechModelInit() 
	{
	    super(); // --> BiomechanicalModelInitializer; super constructs null object
		TissueController.getInstance().getTissueBorder().loadNoMembrane(); // this model has no basement membrane object
		
		// Load global parameters
		MiscalleneousGlobalParameters param = MiscalleneousGlobalParameters.getInstance(); 
		if(param instanceof MiscalleneousGlobalParameters3D)
		{
		    ((MiscalleneousGlobalParameters3D)param).setStandardMembrane_2_Dim_Gauss(false);
			((MiscalleneousGlobalParameters3D)param).setOptimizedGraphics(false);
		}
	}

	// Constructor when loading from saved snapshot
    public FishEyeCenterBasedMechModelInit(SimulationStateData simulationStateData) 
    {
		super(simulationStateData);
		this.simulationStateData = simulationStateData;
	}
	
	// Implementation of abstract method buildStandardInitialCellEnsemble in BiomechanicalModelInitializer
    // Create the distribution of cells in the initial condition of the simulation
	protected ArrayList<UniversalCell> buildStandardInitialCellEnsemble() 
	{
		// Load the default values for cell dimensions from the model at runtime.
        // The variables "WIDTH_DEFAULT", "HEIGHT_DEFAULT", and "LENGTH_DEFAULT" need to be declared in EPISIM Modeller.
        EpisimCellBehavioralModelGlobalParameters cbGP = ModelController.getInstance().getEpisimCellBehavioralModelGlobalParameters();		
		try
		{
	        Field field = cbGP.getClass().getDeclaredField("WIDTH_DEFAULT");
	        CELL_WIDTH = field.getDouble(cbGP);  
	        field = cbGP.getClass().getDeclaredField("HEIGHT_DEFAULT");
	        CELL_HEIGHT = field.getDouble(cbGP);
	        field = cbGP.getClass().getDeclaredField("LENGTH_DEFAULT");
	        CELL_LENGTH = field.getDouble(cbGP);   
        }
        catch (NoSuchFieldException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }
        catch (SecurityException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }
        catch (IllegalArgumentException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }
        catch (IllegalAccessException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }

		// Get the biomechanical global parameters
		FishEyeCenterBased3DModelGP mechModelGP = (FishEyeCenterBased3DModelGP) ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters();
		
		// Set initial hemisphere radius
		mechModelGP.setInnerEyeRadius(mechModelGP.getInitialInnerEyeRadius());
		
		/* 
		 * Estimate how many cells are needed to create a densely packed distribution on the hemispherical and annular surfaces.
		 * Assume that each cell occupies a circular area proportional to its radius scaled by the tolerated overlap.
		 * See also supplementary equation 1 and accompanying text in Tsingos et al 2019.
		 */
		
		// Get the cell's largest dimension (in this model cells are circular, so this is redundant)
		double cellSize = Math.max(CELL_WIDTH, CELL_HEIGHT);
		cellSize = Math.max(cellSize, CELL_LENGTH);
		double cellRadius = cellSize/2d;
		double tol_overlap = 1 - mechModelGP.getLinearToExpMaxOverlap_perc(); // Use the linear to exp overlap threshold to generate tight packing
		double cellArea = Math.PI*Math.pow(cellRadius*(1-tol_overlap),2); // Supp Eq 1 in Tsingos et al.
		
		// Initialize surface areas to be calculated
		double area_inner_hemisphere = 0;
		double area_outer_hemisphere = 0;
		double area_inner_annulus = 0;
		double area_outer_annulus = 0;
		
		// Inner hemisphere area
		double innerRadius = mechModelGP.getInitialInnerEyeRadius();
		Point3d fishEyeCenter = mechModelGP.getInnerEyeCenter();
		double hemisphereArea = 2*Math.PI*Math.pow(innerRadius,2);
		area_inner_hemisphere = hemisphereArea;
		
		// Annulus area
		if (mechModelGP.isAnnulus())
		{
			double annulusArea = Math.PI*( Math.pow(innerRadius, 2) - Math.pow(innerRadius - mechModelGP.getInitialAnnulusWidthMikron(), 2) );
			area_inner_annulus = annulusArea;
		}

		// Outer hemisphere area in bilayer model
		double outerRadius = 0;
		if (mechModelGP.isBilayer())
		{
			mechModelGP.setOuterEyeRadiusOffset(cellSize);
			outerRadius = innerRadius + mechModelGP.getOuterEyeRadiusOffset();
			double outerHemisphereArea = 2*Math.PI*Math.pow(outerRadius,2);
			area_outer_hemisphere = outerHemisphereArea;
			
			if (mechModelGP.isAnnulus())
			{
				double outerAnnulusArea = Math.PI*( Math.pow(outerRadius, 2) - Math.pow(outerRadius - mechModelGP.getInitialAnnulusWidthMikron(), 2) );
				area_outer_annulus = outerAnnulusArea;
			}
		}
		
		// Reduce hemisphere area in the frustum model
		if (mechModelGP.isFrustum())
		{
			// Inner hemisphere
			double frustumHeight = mechModelGP.getFrustumHeightMikron();
			double frustumArea = 2*Math.PI*innerRadius*frustumHeight;
			area_inner_hemisphere = frustumArea;
			
			// Outer hemisphere
			if (mechModelGP.isBilayer())
			{
				double outerFrustumHeight = mechModelGP.getFrustumHeightMikron();
				double outerFrustumArea = 2*Math.PI*outerRadius*outerFrustumHeight;
				area_outer_hemisphere = outerFrustumArea;
			}
		}
		
		// Calculate the number of cells that fit in the respective areas
		int numCellsFitInnerHemisphere = (int) Math.ceil(area_inner_hemisphere/cellArea); // inner hemisphere
		int numCellsFitInnerAnnulus = (int) Math.ceil(area_inner_annulus/cellArea); // inner annulus
		int numCellsFitOuterHemisphere = (int) Math.ceil(area_outer_hemisphere/cellArea); // outer hemisphere
		int numCellsFitOuterAnnulus = (int) Math.ceil(area_outer_annulus/cellArea); // outer annulus
		int sumCells = numCellsFitInnerHemisphere + numCellsFitInnerAnnulus + numCellsFitOuterHemisphere + numCellsFitOuterAnnulus;
		
        /*
		 * To generate a distribution of points onto which the cells will be placed, generate an icosahedral mesh
		 * and check whether the nodes of this mesh fall onto the hemisphere. If they do, a cell is placed there until
		 * the total number of cells calculated above is reached.
		 */
		
		int cell_counter_inner_hemisphere = 0;
		int cell_counter_inner_annulus = 0;
		int cell_counter_outer_hemisphere = 0;
		int cell_counter_outer_annulus = 0;
		HashSet<Point3d> existingCoordinates = new HashSet<Point3d>();
		Point3d previousPoint = null;
		ArrayList<UniversalCell> standardCellEnsemble = new ArrayList<UniversalCell>();
		
		// Generate icosahedral mesh and subdivide it x times
		Icosahedron ico = new Icosahedron(6);
	    int ignoredCells = 0;

		final MersenneTwisterFast random_uniform = new MersenneTwisterFast(System.currentTimeMillis());
	    
	    for ( int i = 0; i < ico.getVertexList().size(); i+=3 )
	    {
	    	int sumCounters = cell_counter_inner_hemisphere + cell_counter_inner_annulus + cell_counter_outer_hemisphere + cell_counter_outer_annulus;
			// Check if total number of cells already instantiated in hemisphere or annulus.
			if (sumCounters <= sumCells)
	    	{
				// Get icosahedral mesh vertex coordinates (normalized) and scale to initial eye radius 
				double x = ico.getVertexList().get(i) * innerRadius;
				double y = ico.getVertexList().get(i+1) * innerRadius;
				double z = ico.getVertexList().get(i+2) * innerRadius;
								
				// Translate coordinates to be centered around fishEyeCenter
				Point3d newPos = new Point3d(fishEyeCenter.x + x, fishEyeCenter.y + y, fishEyeCenter.z + z);
				
				// Check if this new point already exists AND if the previous point is null OR the new point is at a distance greater than a cell from the new point (prevents costly calculations)
				if ( !existingCoordinates.contains(newPos) && (previousPoint == null || previousPoint.distance(newPos)>cellSize) )
				{
					boolean skipCell = false;
					// If the eye globe is truncated as a frustum, check that no cell falls beyond the frustum height threshold.
					if (mechModelGP.isFrustum() && newPos.x < (fishEyeCenter.x + mechModelGP.getFrustumHeightMikron()))
					{
						skipCell = true;
						ignoredCells++;
					}

					boolean randomAssignInner = true;
					if (mechModelGP.isBilayer())
					{
						randomAssignInner = random_uniform.nextBoolean(0.5);
					}
					
					// Initialize cells on hemisphere if x-coordinate is > fish eye center
					if (newPos.x >= (fishEyeCenter.x - cellSize) && (cell_counter_inner_hemisphere+cell_counter_outer_hemisphere) <= (numCellsFitInnerHemisphere+numCellsFitOuterHemisphere) && !skipCell)
					{
						if (cell_counter_inner_hemisphere <= numCellsFitInnerHemisphere && randomAssignInner)
						{
							cell_counter_inner_hemisphere += 1;
							
							UniversalCell stemCell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
							// Assign the biomechanical model and set dimensions
							FishEyeCenterBased3DModel mechModel = ((FishEyeCenterBased3DModel) stemCell.getEpisimBioMechanicalModelObject());		
							mechModel.setCellWidth(CELL_WIDTH);
							mechModel.setCellHeight(CELL_HEIGHT);
							mechModel.setCellLength(CELL_LENGTH);
							mechModel.setStandardCellWidth(CELL_WIDTH);
							mechModel.setStandardCellHeight(CELL_HEIGHT);
							mechModel.setStandardCellLength(CELL_LENGTH);
							
							// Add the new cell to the standard cell ensemble
							existingCoordinates.add(newPos);
							previousPoint = newPos;
							mechModel.setPositionRespectingBounds(newPos, CELL_WIDTH/2d, CELL_HEIGHT/2d, CELL_LENGTH/2d, mechModelGP.getOptDistanceToBMScalingFactor(), true);			
							
							EpisimModelConnector mc = mechModel.getEpisimModelConnector();
							if(mc != null && mc instanceof EpisimFishEyeCenterBased3DMC){
								EpisimFishEyeCenterBased3DMC modelConnector = (EpisimFishEyeCenterBased3DMC) mc;
								modelConnector.setIsHemisphere(true);
								modelConnector.setIsInner(true);
							}
							standardCellEnsemble.add(stemCell);
						}
						else if (cell_counter_outer_hemisphere <= numCellsFitOuterHemisphere && !randomAssignInner)
						{
							cell_counter_outer_hemisphere += 1;
							
							UniversalCell stemCell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
							// Assign the biomechanical model and set dimensions
							FishEyeCenterBased3DModel mechModel = ((FishEyeCenterBased3DModel) stemCell.getEpisimBioMechanicalModelObject());		
							mechModel.setCellWidth(CELL_WIDTH);
							mechModel.setCellHeight(CELL_HEIGHT);
							mechModel.setCellLength(CELL_LENGTH);
							mechModel.setStandardCellWidth(CELL_WIDTH);
							mechModel.setStandardCellHeight(CELL_HEIGHT);
							mechModel.setStandardCellLength(CELL_LENGTH);
							
							// Correct coordinates to be on the outer hemisphere radius
							y = ico.getVertexList().get(i+1) * outerRadius;
							z = ico.getVertexList().get(i+2) * outerRadius;
							newPos = new Point3d(fishEyeCenter.x + x, fishEyeCenter.y + y, fishEyeCenter.z + z);
							
							// Add the new cell to the standard cell ensemble
							existingCoordinates.add(newPos);
							previousPoint = newPos;
							mechModel.setPositionRespectingBounds(newPos, CELL_WIDTH/2d, CELL_HEIGHT/2d, CELL_LENGTH/2d, mechModelGP.getOptDistanceToBMScalingFactor(), true);			
							
							EpisimModelConnector mc = mechModel.getEpisimModelConnector();
							if(mc != null && mc instanceof EpisimFishEyeCenterBased3DMC){
								EpisimFishEyeCenterBased3DMC modelConnector = (EpisimFishEyeCenterBased3DMC) mc;
								modelConnector.setIsHemisphere(true);
								modelConnector.setIsInner(false);
							}
							standardCellEnsemble.add(stemCell);
						}
					}
					// initialize cells on annulus
					else if (mechModelGP.isAnnulus() && newPos.x < fishEyeCenter.x && (cell_counter_inner_annulus+cell_counter_outer_annulus) <= (numCellsFitInnerAnnulus+numCellsFitOuterAnnulus))
					{
//						// Correct coordinates
//						double lensRadius = mechModelGP.getInnerEyeRadius() - mechModelGP.getAnnulusWidthMikron();
//						double min = lensRadius + mechModelGP.getBilayerCellDiameterFactor()*cellRadius;
//						double max = outerRadius;
//	   	   	    		double scale = min + random_uniform.nextDouble()*(max-min);
//		   	   	    	Vector3d rayYZ = new Vector3d(0, (newPos.y-fishEyeCenter.y), (newPos.z-fishEyeCenter.z));
//		   	   	    	if (rayYZ.length() < lensRadius + mechModelGP.getBilayerCellDiameterFactor()*cellRadius )
//		   	   	    	{
//		   	   	    		rayYZ.normalize();
//		   	   	    		rayYZ.scale(scale);
//		   	   	    		newPos.y = fishEyeCenter.y + rayYZ.y;
//		   	   	    		newPos.z = fishEyeCenter.z + rayYZ.z;
//		   	   	    	}
//		   	   	    	newPos.x -= cellRadius; // plane of annulus
						
						if (cell_counter_inner_annulus <= numCellsFitInnerAnnulus && randomAssignInner)
						{
							cell_counter_inner_annulus += 1;
							
							UniversalCell stemCell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
							// Assign the biomechanical model and set dimensions
							FishEyeCenterBased3DModel mechModel = ((FishEyeCenterBased3DModel) stemCell.getEpisimBioMechanicalModelObject());		
							mechModel.setCellWidth(CELL_WIDTH);
							mechModel.setCellHeight(CELL_HEIGHT);
							mechModel.setCellLength(CELL_LENGTH);
							mechModel.setStandardCellWidth(CELL_WIDTH);
							mechModel.setStandardCellHeight(CELL_HEIGHT);
							mechModel.setStandardCellLength(CELL_LENGTH);
							
							// Add the new cell to the standard cell ensemble
							existingCoordinates.add(newPos);
							previousPoint = newPos;
							mechModel.setPositionRespectingBounds(newPos, CELL_WIDTH/2d, CELL_HEIGHT/2d, CELL_LENGTH/2d, mechModelGP.getOptDistanceToBMScalingFactor(), true);
							EpisimModelConnector mc = mechModel.getEpisimModelConnector();
							if(mc != null && mc instanceof EpisimFishEyeCenterBased3DMC){
								EpisimFishEyeCenterBased3DMC modelConnector = (EpisimFishEyeCenterBased3DMC) mc;
								modelConnector.setIsAnnulus(true);
								modelConnector.setIsInner(true);
							}
							standardCellEnsemble.add(stemCell);
						}
						else if (cell_counter_outer_annulus <= numCellsFitOuterAnnulus && !randomAssignInner)
						{
							cell_counter_outer_annulus += 1;
							
							UniversalCell stemCell = new UniversalCell(null, null, true); // instantiate a cell with no mother cell, and no cell behavioural model
							// Assign the biomechanical model and set dimensions
							FishEyeCenterBased3DModel mechModel = ((FishEyeCenterBased3DModel) stemCell.getEpisimBioMechanicalModelObject());		
							mechModel.setCellWidth(CELL_WIDTH);
							mechModel.setCellHeight(CELL_HEIGHT);
							mechModel.setCellLength(CELL_LENGTH);
							mechModel.setStandardCellWidth(CELL_WIDTH);
							mechModel.setStandardCellHeight(CELL_HEIGHT);
							mechModel.setStandardCellLength(CELL_LENGTH);
							
							// Add the new cell to the standard cell ensemble
							existingCoordinates.add(newPos);
							previousPoint = newPos;
							mechModel.setPositionRespectingBounds(newPos, CELL_WIDTH/2d, CELL_HEIGHT/2d, CELL_LENGTH/2d, mechModelGP.getOptDistanceToBMScalingFactor(), true);
							EpisimModelConnector mc = mechModel.getEpisimModelConnector();
							if(mc != null && mc instanceof EpisimFishEyeCenterBased3DMC){
								EpisimFishEyeCenterBased3DMC modelConnector = (EpisimFishEyeCenterBased3DMC) mc;
								modelConnector.setIsAnnulus(true);
								modelConnector.setIsInner(false);
							}
							standardCellEnsemble.add(stemCell);
						}
					}
				}
			}
			else ignoredCells++;
		}
	    
	    // Once all the cells have been placed, calculate the force balance to distribute them
		FishEyeCenterBased3DModel.setDummyCellSize(cellSize);
		
		// TODO quick and dirty fix
//		mechModelGP.setRandomness(0.5);
		initializeBiomechanics(standardCellEnsemble);
//		mechModelGP.setRandomness(0.0);
		
		// Define initial differentiation levels, create dummy cells
		setDiffLevels(standardCellEnsemble, cellSize);
		System.out.println("Cells that fit on inner hemisphere:" + numCellsFitInnerHemisphere + ", on inner annulus: " + numCellsFitInnerAnnulus + ", on outer hemisphere: " + numCellsFitOuterHemisphere+ ", on outer annulus: " + numCellsFitOuterAnnulus);
		System.out.println("No of cells: " + standardCellEnsemble.size()+ "\nCells Ignored: " + ignoredCells + "\nNumber of vertices: " + ico.getVertexList().size());
		return standardCellEnsemble;
	}
	
    private void setDiffLevels(ArrayList<UniversalCell> standardCellEnsemble, double cellSize)
	{
	    FishEyeCenterBased3DModelGP mechModelGP = (FishEyeCenterBased3DModelGP) ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters();
		EpisimDifferentiationLevel[] diffLevels = ModelController.getInstance().getCellBehavioralModelController().getAvailableDifferentiationLevels();
		
		double prolifBeltSize = mechModelGP.getProlifCompWidthMikron();
		double radius = mechModelGP.getInitialInnerEyeRadius();
		
		// The following is used to calculate the x interval within which initialized cells are assigned to diffLevel[1] (proliferative)
		// The angle subtended by the proliferative belt can be calculated for the 2D case due to symmetry
		// The proliferative belt spans the distance as measured by a ruler (projected distance)
		double prolifBeltAngle = Math.PI/2d - Math.acos(prolifBeltSize/radius);
		double xDelta = radius * prolifBeltAngle; // xDelta is the arc given by the angle
		
		// Loop through cells and set differentiation levels: Within the belt = proliferative, beyond the belt = differentiated
		for(int i=0; i < standardCellEnsemble.size(); i++)
		{	
			UniversalCell actCell = standardCellEnsemble.get(i);
			EpisimBiomechanicalModel biomech = actCell.getEpisimBioMechanicalModelObject();
			
			if(biomech instanceof FishEyeCenterBased3DModel)
			{
				if(diffLevels.length>2)
				{
			        if((biomech.getX()+(cellSize/2d)) <= (mechModelGP.getInnerEyeCenter().x + xDelta)) // Cell in the belt
			        {
						actCell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[1]); // Proliferative cell
					}
					else // Cell out of the belt
					{
						actCell.getEpisimCellBehavioralModelObject().setDiffLevel(diffLevels[2]); // Differentiated cell
					}
				}
			}
		}
	}
	
	// This part is the initial mechanical relaxation of the model, before the proper simulation starts
	private void initializeBiomechanics(ArrayList<UniversalCell> standardCellEnsemble)
	{
		EpisimBiomechanicalModel biomech = ModelController.getInstance().getBioMechanicalModelController().getNewEpisimBioMechanicalModelObject(null, null);
		FishEyeCenterBased3DModelGP mechModelGP = (FishEyeCenterBased3DModelGP) ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters();
		
		if(biomech instanceof FishEyeCenterBased3DModel)
		{
			double itermax = 50;
			double i = 1;
			double randomness_initial = 0.25;
			double randomness = randomness_initial;
			mechModelGP.setRandomness(randomness_initial);
			FishEyeCenterBased3DModel cbBioMech = (FishEyeCenterBased3DModel) biomech;			
			double cumulativeMigrationDist = 0;
			do
			{
				cbBioMech.initialisationGlobalSimStep(); // Dummy cells generated in the downstream function
				cumulativeMigrationDist = getCumulativeMigrationDistance(standardCellEnsemble);
				randomness = randomness_initial*(1-(i/itermax)) + 0.01;
				mechModelGP.setRandomness(randomness);
				System.out.println((cumulativeMigrationDist / standardCellEnsemble.size()));
				System.out.println("iteration: " + i + "\t randomness: " + randomness);
				i++;
			}
			// Continue calculating until cells have reached a stable distribution (cumulative migration distance < threshold)
			while(standardCellEnsemble.size() > 0 && ((cumulativeMigrationDist / standardCellEnsemble.size()) > mechModelGP.getMinAverageMigrationMikron()) && i <= itermax);
		}
	}
	
	// Get migration of cells during relaxation
	private double getCumulativeMigrationDistance(ArrayList<UniversalCell> standardCellEnsemble)
	{
		double cumulativeMigrationDistance = 0;
		
		for(int i = 0; i < standardCellEnsemble.size(); i++)
		{
			FishEyeCenterBased3DModel mechModel = ((FishEyeCenterBased3DModel) standardCellEnsemble.get(i).getEpisimBioMechanicalModelObject());
			cumulativeMigrationDistance += mechModel.getMigrationDistPerSimStep();
		}
		return cumulativeMigrationDistance;
	}
	
 	/*
 	 *  This function is used when loading a simulation state from a snapshot.
 	 *  It does essentially the same as the function "buildStandardInitialCellEnsemble"
 	 *  but doesn't need to generate points and calculate an initial mechanical relaxation.
 	 */
	protected ArrayList<UniversalCell> buildInitialCellEnsemble()
	{
        ArrayList<UniversalCell> loadedCells = super.buildInitialCellEnsemble();
		EpisimCellBehavioralModelGlobalParameters cbGP = ModelController.getInstance().getEpisimCellBehavioralModelGlobalParameters();		
		
		try
		{
	        Field field = cbGP.getClass().getDeclaredField("WIDTH_DEFAULT");
	        CELL_WIDTH = field.getDouble(cbGP);
	        field = cbGP.getClass().getDeclaredField("HEIGHT_DEFAULT");
	        CELL_HEIGHT = field.getDouble(cbGP);
	        field = cbGP.getClass().getDeclaredField("LENGTH_DEFAULT");
	        CELL_LENGTH = field.getDouble(cbGP);   
        }
        catch (NoSuchFieldException e)
        {
            EpisimExceptionHandler.getInstance().displayException(e);
        }
        catch (SecurityException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }
        catch (IllegalArgumentException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }
        catch (IllegalAccessException e)
        {
      	    EpisimExceptionHandler.getInstance().displayException(e);
        }			
		
		double cellSize = Math.max(CELL_WIDTH, CELL_HEIGHT);
		cellSize = Math.max(cellSize, CELL_LENGTH);
		FishEyeCenterBased3DModel.setDummyCellSize(cellSize);
		
		return loadedCells;
	}

	protected void initializeCellEnsembleBasedOnRandomAgeDistribution(ArrayList<UniversalCell> cellEnsemble)
	{
		// This method has to be implemented but has nothing to do in this model
	}

	/*
	 * The portrayal functions instantiate the 3D representation of the cells.
	 */
	protected EpisimPortrayal getCellPortrayal() 
	{
		ContinuousCellFieldPortrayal3D continuousPortrayal = new ContinuousCellFieldPortrayal3D("Fish Eye");
		continuousPortrayal.setField(ModelController.getInstance().getBioMechanicalModelController().getCellField());
		return continuousPortrayal;
	}

	protected EpisimPortrayal[] getAdditionalPortrayalsCellForeground() 
	{
		ContinuousCellFieldPortrayal3D continuousPortrayal = new ContinuousCellFieldPortrayal3D("Dummy Cells");
		continuousPortrayal.setField(FishEyeCenterBased3DModel.getDummyCellField());
		return new EpisimPortrayal[]{continuousPortrayal};
	}

	protected EpisimPortrayal[] getAdditionalPortrayalsCellBackground() 
	{
		return new EpisimPortrayal[0];
	}
}