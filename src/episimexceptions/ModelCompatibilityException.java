package episimexceptions;


public class ModelCompatibilityException extends Exception{
	
	public ModelCompatibilityException(String msg){
		super(msg);
	}

}
