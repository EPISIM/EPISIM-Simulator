package episimexceptions;


public class DataMonitoringException extends RuntimeException {
	
	public DataMonitoringException(String msg){
		super(msg);
	}

}