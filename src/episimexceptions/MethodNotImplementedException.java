package episimexceptions;


public class MethodNotImplementedException extends RuntimeException {
	
	public MethodNotImplementedException(String message){
		super(message);
	}

}
