package episimexceptions;


public class ObjectsForChartingMissingException extends Exception{
	public ObjectsForChartingMissingException(String msg){
		super(msg);
	}
}
