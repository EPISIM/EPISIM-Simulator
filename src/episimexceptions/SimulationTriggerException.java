package episimexceptions;


public class SimulationTriggerException extends Exception {
	public SimulationTriggerException(String msg){
		super(msg);
	}
}
