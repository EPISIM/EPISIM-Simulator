package episimexceptions;


public class PropertyException extends RuntimeException {
	
	public PropertyException(String msg){
		super(msg);
	}
}