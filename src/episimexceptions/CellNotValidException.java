package episimexceptions;


public class CellNotValidException extends Exception {
	
	public CellNotValidException(String msg){
		super(msg);
	}

}
