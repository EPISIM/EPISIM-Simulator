package episimexceptions;


public class CompilationFailedException extends Exception {
	public CompilationFailedException(String msg){
		super(msg);
	}
}
