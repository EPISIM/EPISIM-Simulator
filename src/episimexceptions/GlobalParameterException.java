package episimexceptions;


public class GlobalParameterException extends RuntimeException {
		
		public GlobalParameterException(String msg){
			super(msg);
		}
}