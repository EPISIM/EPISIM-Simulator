package episimexceptions;


public class MissingObjectsException extends Exception {
	
	public MissingObjectsException(String msg){
		super(msg);
	}

}
