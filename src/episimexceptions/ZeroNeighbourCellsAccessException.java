package episimexceptions;

public class ZeroNeighbourCellsAccessException extends RuntimeException{
	
	public ZeroNeighbourCellsAccessException(String msg){
		super(msg);
	}
}
