package sim.app.episim.model.biomechanics.centerbased3d.tcellmedaka.students;

/*
 Contains all global biomechanical model parameters.
 Implements methods to get and set these parameters.
 */

import javax.vecmath.Point3d;

import episiminterfaces.EpisimBiomechanicalModelGlobalParameters;
import episiminterfaces.NoUserModification;

public class TCellMedakaCenterBased3DModelGP implements EpisimBiomechanicalModelGlobalParameters, java.io.Serializable 
{
	
	// diameters of thymus ellipsoid
	
	private double thymusSliceFactor = 0.1; // scaling factor for pseudo-2D model
	
	private double thymusLength = 50 * thymusSliceFactor; // units: [um]
	private double thymusWidth = 100; // units: [um]
	private double thymusHeight = 50; // units: [um]
	
    private double width = 125; // units: [um]
	private double height = 80; // units: [um]
	private double length = 60; // units: [um]
	
	private double thymusInnerCoreFraction = 0.7; // Fraction of thymus ellipsoid occupied by medulla; units: [unitless]; value range: 0-1
	
	private double thymusCenterX = width/2; // units: [um]
	private double thymusCenterY = 2*height/3; // units: [um]
	private double thymusCenterZ = length/2; // units: [um]
		
	private double radiusTEC = 2.5; // units: [um]
	private double radiusTSP_source = 0.5; // units: [um]
	private double radiusTSP = 2.0; // units: [um]
	
	/*
	 * In the pseudo 2D model version, TEC initial positions are calculated based on 
	 * concentric shells of the radius.
	 * An exclusion zone at the edges is set. 
	 */
	private int tec_nShells = 3; // units: unitless
	private double tec_ShellRadiusFactor = 0.125; // units: unitless
	private double tec_SubdivisionFactor = 0.4; // units: unitless
	private double teclet_angleFactor = 0.1; // units: unitless
	private int tec_shellSubdivisions = 3; // units: unitless
	private double tec_centralExclusionFraction = 0.1;
	private int tec_Arms = 4; // units: [unitless] - number of TEC arms
	private int tecletsPerTEC = 2; // units: [unitless] - number of TEClets for each arm
	private double tecletDistanceFactor = 0.9; // units: [unitless]
	private double minimumTecletSize = 1; // units: [um]

	private double tec_biomechanics_factor = 0; // multiplier for TEC and teclet biomechanics calculation
		 
	private int numberInitialTSPsource = (int) (20 * thymusSliceFactor) > 1 ? (int) (20 * thymusSliceFactor) : 2; // units: [unitless]
	private double fishTSPsourceDomainSize = 5; // units: [um]
	
	private int initialization_seed = 42; // random seed for initializing TEC and TSP_source locations
	
	private double minAverageMigrationMikron = 0.5; // units: [um]
		
	private double neighborhood_mikron = 10.0; // units: [um]
	private double numberOfPixelsPerMicrometer = 1; // units: [pixel/um]
	private int numberOfSecondsPerSimStep = 30; // units: [s]
		
	private double mechanicalNeighbourhoodOptDistFact = 1.5; // units: [unitless]
	private double directNeighbourhoodOptDistFact = 1.3; // units: [unitless]
	private double optDistanceAdhesionFact = 1.3; // units: [unitless]
	private double optDistanceScalingFactor = 0.85; // units: [unitless]
	private double linearToExpMaxOverlap_Perc = 0.5; // units: [unitless]
	private double repulSpringStiffness_N_per_micro_m = 2.2E-9; /// units: [N/um]
	private double frictionMedium = 4E-7; // units: [Ns/um] ; reference: Galle, Loeffler Drasdo 2005 epithelial cells 0.4 Ns/m (* 10^-6 for conversion to micron )
	private double adhSpringStiffness_N_per_square_micro_m = 2.2E-11; //units: [N/um^2]
	
	private int neighbourLostThres = 10; // units: [simulation steps]
	
	public TCellMedakaCenterBased3DModelGP() {}	
	
	@NoUserModification
	public double getNeighborhood_mikron() { return neighborhood_mikron; }
	
	@NoUserModification
 	public void setNeighborhood_mikron(double val) { if (val > 0) neighborhood_mikron= val; }	
	
	public void setWidthInMikron(double val) {
		if(val > 0)	width = val;
	}	
	
	public double getWidthInMikron() {
		return width;
	}
	
    public double getHeightInMikron() {   
   	    return height;
    }	
   
    public void setHeightInMikron(double val) {   
   	    if(val > 0) this.height = val;
    }
   
    public double getLengthInMikron() {   	
   	    return this.length;
    }	
   
    public void setLengthInMikron(double val) {   
   	    if(val > 0) this.length = val;
    }

	public void setNumberOfPixelsPerMicrometer(double val) {
		this.numberOfPixelsPerMicrometer = val;
    }
	
	@NoUserModification
	public double getNumberOfPixelsPerMicrometer() {
		return this.numberOfPixelsPerMicrometer;
    }

	@NoUserModification
    public ModelDimensionality getModelDimensionality() {	   
	    return ModelDimensionality.THREE_DIMENSIONAL;
    }

	@NoUserModification
    public double getMechanicalNeighbourhoodOptDistFact() {   
   	    return mechanicalNeighbourhoodOptDistFact;
    }

	@NoUserModification
    public void setMechanicalNeighbourhoodOptDistFact(double neighbourhoodOptDistFact) {
   	    this.mechanicalNeighbourhoodOptDistFact = neighbourhoodOptDistFact;
    }
	
    public double getDirectNeighbourhoodOptDistFact() {
      	return directNeighbourhoodOptDistFact;
    }

	@NoUserModification
    public void setDirectNeighbourhoodOptDistFact(double directNeighbourhoodOptDistFact) {
        if(directNeighbourhoodOptDistFact>=1)this.directNeighbourhoodOptDistFact = directNeighbourhoodOptDistFact;
    }
	
    public double getOptDistanceAdhesionFact() {
   	    return optDistanceAdhesionFact;
    }

    public void setOptDistanceAdhesionFact(double optDistanceAdhesionFact) {
   	    this.optDistanceAdhesionFact = optDistanceAdhesionFact>= 1?optDistanceAdhesionFact:1;
    }

    public double getOptDistanceScalingFactor() {
   	    return optDistanceScalingFactor;
    }

    public void setOptDistanceScalingFactor(double optDistanceScalingFactor) {
        this.optDistanceScalingFactor = optDistanceScalingFactor > 0 ? optDistanceScalingFactor: 0.001;
    }  
	
    public double getLinearToExpMaxOverlap_perc() {
        return linearToExpMaxOverlap_Perc;
    }

    public void setLinearToExpMaxOverlap_perc(double linearToExpMaxOverlap_Perc) {
        this.linearToExpMaxOverlap_Perc = linearToExpMaxOverlap_Perc;
    }

    public double getRepulSpringStiffness_N_per_micro_m() {
        return repulSpringStiffness_N_per_micro_m;
    }

    public void setRepulSpringStiffness_N_per_micro_m(double repulSpringStiffness_N_per_micro_m) {
        this.repulSpringStiffness_N_per_micro_m = repulSpringStiffness_N_per_micro_m;
    }
    
    public double getAdhSpringStiffness_N_per_square_micro_m() {
        return adhSpringStiffness_N_per_square_micro_m;
    }

    public void setAdhSpringStiffness_N_per_square_micro_m(double adhSpringStiffness_N_per_square_micro_m) {
        this.adhSpringStiffness_N_per_square_micro_m = adhSpringStiffness_N_per_square_micro_m;
    }

    public int getNumberOfSecondsPerSimStep() {   
   	    return numberOfSecondsPerSimStep;
    }

    public void setNumberOfSecondsPerSimStep(int numberOfSecondsPerSimStep) {   
   	    if(numberOfSecondsPerSimStep>0)this.numberOfSecondsPerSimStep = numberOfSecondsPerSimStep;
    }

    public int getNeighbourLostThres() {
        return neighbourLostThres;
    }

    public void setNeighbourLostThres(int neighbourLostThres) {
        this.neighbourLostThres = neighbourLostThres;
    }

    public double getMinAverageMigrationMikron() {
    	return minAverageMigrationMikron;
    }
 
    public void setMinAverageMigrationMikron(double minAverageMigrationMikron) {
        this.minAverageMigrationMikron = minAverageMigrationMikron;
    }

	public double getFrictionMedium() {
		return frictionMedium;
	}

	public void setFrictionMedium(double frictionMedium) {
		this.frictionMedium = frictionMedium;
	}

	public double getThymusWidth() {
		return thymusWidth;
	}

	public void setThymusWidth(double thymusWidth) {
		this.thymusWidth = thymusWidth;
	}

	public double getThymusHeight() {
		return thymusHeight;
	}

	public void setThymusHeight(double thymusHeight) {
		this.thymusHeight = thymusHeight;
	}

	public double getThymusLength() {
		return thymusLength;
	}

	public void setThymusLength(double thymusLength) {
		this.thymusLength = thymusLength;
	}

	public double getThymusCenterX() {
		return thymusCenterX;
	}

	public void setThymusCenterX(double thymusCenterX) {
		this.thymusCenterX = thymusCenterX;
	}

	public double getThymusCenterY() {
		return thymusCenterY;
	}

	public void setThymusCenterY(double thymusCenterY) {
		this.thymusCenterY = thymusCenterY;
	}

	public double getThymusCenterZ() {
		return thymusCenterZ;
	}

	public void setThymusCenterZ(double thymusCenterZ) {
		this.thymusCenterZ = thymusCenterZ;
	}

	public double getThymusInnerCoreFraction() {
		return thymusInnerCoreFraction;
	}

	public void setThymusInnerCoreFraction(double thymusInnerCoreFraction) {
		this.thymusInnerCoreFraction = thymusInnerCoreFraction;
	}

	
	public int getNumberInitialTSPsource() {
		return numberInitialTSPsource;
	}

	public void setNumberInitialTSPsource(int numberInitialTSPsource) {
		this.numberInitialTSPsource = numberInitialTSPsource;
	}

	public int getInitialization_seed() {
		return initialization_seed;
	}

	public void setInitialization_seed(int initialization_seed) {
		this.initialization_seed = initialization_seed;
	}

	public double getRadiusTEC() {
		return radiusTEC;
	}

	public void setRadiusTEC(double radiusTEC) {
		this.radiusTEC = radiusTEC;
	}

	public double getRadiusTSP_source() {
		return radiusTSP_source;
	}

	public void setRadiusTSP_source(double radiusTSP_source) {
		this.radiusTSP_source = radiusTSP_source;
	}

	public double getRadiusTSP() {
		return radiusTSP;
	}

	public void setRadiusTSP(double radiusTSP) {
		this.radiusTSP = radiusTSP;
	}

	public int getTec_nShells() {
		return tec_nShells;
	}

	public void setTec_nShells(int tec_nShells) {
		this.tec_nShells = tec_nShells;
	}

	public double getTec_ShellRadiusFactor() {
		return tec_ShellRadiusFactor;
	}

	public void setTec_ShellRadiusFactor(double tec_ShellRadiusFactor) {
		this.tec_ShellRadiusFactor = tec_ShellRadiusFactor;
	}

	public int getTec_Arms() {
		return tec_Arms;
	}

	public void setTec_Arms(int tec_Arms) {
		this.tec_Arms = tec_Arms;
	}

	public double getTecletDistanceFactor() {
		return tecletDistanceFactor;
	}

	public void setTecletDistanceFactor(double tecletDistanceFactor) {
		this.tecletDistanceFactor = tecletDistanceFactor;
	}

	public double getFishTSPsourceDomainSize() {
		return fishTSPsourceDomainSize;
	}

	public void setFishTSPsourceDomainSize(double fishTSPsourceDomainSize) {
		this.fishTSPsourceDomainSize = fishTSPsourceDomainSize;
	}

	public int getTecletsPerTEC() {
		return tecletsPerTEC;
	}

	public void setTecletsPerTEC(int tecletsPerTEC) {
		this.tecletsPerTEC = tecletsPerTEC;
	}

	public double getMinimumTecletSize() {
		return minimumTecletSize;
	}

	public void setMinimumTecletSize(double minimumTecletSize) {
		this.minimumTecletSize = minimumTecletSize;
	}

	public int getTec_shellSubdivisions() {
		return tec_shellSubdivisions;
	}

	public void setTec_shellSubdivisions(int tec_shellSubdivisions) {
		this.tec_shellSubdivisions = tec_shellSubdivisions;
	}

	public double getTec_centralExclusionFraction() {
		return tec_centralExclusionFraction;
	}

	public void setTec_centralExclusionFraction(double tec_centralExclusionFraction) {
		this.tec_centralExclusionFraction = tec_centralExclusionFraction;
	}

	public double getTec_SubdivisionFactor() {
		return tec_SubdivisionFactor;
	}

	public void setTec_SubdivisionFactor(double tec_SubdivisionFactor) {
		this.tec_SubdivisionFactor = tec_SubdivisionFactor;
	}

	public double getTeclet_angleFactor() {
		return teclet_angleFactor;
	}

	public void setTeclet_angleFactor(double teclet_angleFactor) {
		this.teclet_angleFactor = teclet_angleFactor;
	}

	public double getTec_biomechanics_factor() {
		return tec_biomechanics_factor;
	}

	public void setTec_biomechanics_factor(double tec_biomechanics_factor) {
		this.tec_biomechanics_factor = tec_biomechanics_factor;
	}
}