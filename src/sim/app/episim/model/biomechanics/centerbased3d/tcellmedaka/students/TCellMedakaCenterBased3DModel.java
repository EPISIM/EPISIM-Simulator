package sim.app.episim.model.biomechanics.centerbased3d.tcellmedaka.students;

/*
 * Biomechanical model used at every simulation step
 */

import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.media.j3d.Shape3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Matrix3d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import sim.app.episim.EpisimProperties;
import sim.app.episim.model.AbstractCell;
import sim.app.episim.model.biomechanics.CellBoundaries;
import sim.app.episim.model.biomechanics.Ellipsoid;
import sim.app.episim.model.biomechanics.Episim3DCellShape;
import sim.app.episim.model.biomechanics.centerbased3d.AbstractCenterBased3DModel;
import sim.app.episim.model.controller.ModelController;
import sim.app.episim.model.controller.TissueController;
import sim.app.episim.util.GenericBag;
import sim.app.episim.util.Loop;
import sim.app.episim.visualization.EpisimDrawInfo;
import sim.engine.SimState;
import sim.field.continuous.Continuous3DExt;
import sim.util.Bag;
import sim.util.Double3D;
import ec.util.MersenneTwisterFast;
import episimexceptions.GlobalParameterException;
import episiminterfaces.EpisimCellShape;
import episiminterfaces.NoExport;
import episiminterfaces.monitoring.CannotBeMonitored;
import episimmcc.EpisimModelConnector;
import episimmcc.centerbased3d.tcellmedaka.students.EpisimTCellMedakaCenterBased3DMC;


public class TCellMedakaCenterBased3DModel extends AbstractCenterBased3DModel
{
    // Fields
	private double MIN_OVERLAP_MICRON = 0.1; // minimum deviation from optimal distance considered significant
	   
    private double standardCellWidth = 0; 
    private double standardCellHeight = 0; 
    private double standardCellLength = 0;
   
    private InteractionResult finalInteractionResult;
      
    private double average_overlap;
     
    private EpisimTCellMedakaCenterBased3DMC modelConnector;
   
    private TCellMedakaCenterBased3DModelGP globalParameters = null;
   
    private double migrationDistWholeSimStep = 0;
   
	private static Continuous3DExt cellField;
	
    private static double FIELD_RESOLUTION_IN_MIKRON = 7;
    private static double DELTA_TIME_IN_SECONDS_PER_EULER_STEP = 36; // max biomechanical step size
    private static final double DELTA_TIME_IN_SECONDS_PER_EULER_STEP_MAX = 36;
    private static final double MAX_DISPLACEMENT = 10;
   
    private Double3D cellLocation = null;
    private Double3D newCellLocation = null;
    private Double3D oldCellLocation = null;
    private GenericBag<AbstractCell> directNeighbours;
    private HashSet<Long> directNeighbourIDs;
    private HashMap<Long, Integer> lostNeighbourContactInSimSteps;
   
    // Null constructor
	public TCellMedakaCenterBased3DModel()
	{
   	    this(null, null); // no cell and no model connector assigned
    }
   
	// Constructor for new cells
    public TCellMedakaCenterBased3DModel(AbstractCell cell, EpisimModelConnector modelConnector)
    {
   	    super(cell, modelConnector);
   	    // Representations of space are called 'fields' in MASON parlance.
   	    // All cells exist in cell fields. If there is none defined, instantiate one.
   	    if(cellField == null)
   	    {
   		    cellField = new Continuous3DExt(FIELD_RESOLUTION_IN_MIKRON / 1.5, 
													  TissueController.getInstance().getTissueBorder().getWidthInMikron(), 
													  TissueController.getInstance().getTissueBorder().getHeightInMikron(),
													  TissueController.getInstance().getTissueBorder().getLengthInMikron());   		
   	    }
   	    
   	    // Get global parameters if not instantiated, but only if correct mechanical model loaded
     	if(globalParameters == null)
     	{
	  	    if(ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters() instanceof TCellMedakaCenterBased3DModelGP)
	  	    {
	  		    globalParameters = (TCellMedakaCenterBased3DModelGP) ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters();
	  		}   	
	  		else 
	  		    throw new GlobalParameterException("Datatype of Global Mechanical Model Parameters does not fit : "+
	  		 			ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters().getClass().getName());
        }
     	
        // Wobble interval of initial cell position by applying division bias
     	// Default division bias value has to be set
     	double deltaX = TissueController.getInstance().getActEpidermalTissue().random.nextDouble()*0.005-0.0025;
        double deltaY = TissueController.getInstance().getActEpidermalTissue().random.nextDouble()*0.005-0.0025; 
        double deltaZ = TissueController.getInstance().getActEpidermalTissue().random.nextDouble()*0.005-0.0025; 
        
     	// If cell exists and has a mother cell (cells generated at the initial condition have no mother cell)
        if(cell != null && cell.getMotherCell() != null)
        {
	        if(cell.getMotherCell().getEpisimBioMechanicalModelObject() instanceof TCellMedakaCenterBased3DModel)
	        {
	      	    EpisimModelConnector motherCellConnector = ((TCellMedakaCenterBased3DModel) cell.getMotherCell().getEpisimBioMechanicalModelObject()).getEpisimModelConnector();
	      	    if(motherCellConnector instanceof EpisimTCellMedakaCenterBased3DMC)
	      	    {    		    	    	        
	      		    // Overwrite default division bias values with values obtained from simulation at runtime
	      		    deltaX = ((EpisimTCellMedakaCenterBased3DMC) motherCellConnector).getBiasX();
	    	        deltaY = ((EpisimTCellMedakaCenterBased3DMC) motherCellConnector).getBiasY();
	    	        deltaZ = ((EpisimTCellMedakaCenterBased3DMC) motherCellConnector).getBiasZ();
	      	    }
	        } 

	        // Place newly-generated cell on mother cell location + division bias
	        Double3D oldLoc = cellField.getObjectLocation(cell.getMotherCell());
	        if(oldLoc != null)
	        {
		        Double3D newloc = new Double3D(oldLoc.x + deltaX, oldLoc.y + deltaY, oldLoc.z + deltaZ);
		        cellLocation = newloc;
		        cellField.setObjectLocation(cell, newloc);		      
	        }
        }
        if(modelConnector != null)
        {
		    setEpisimModelConnector(modelConnector);
		}
        // Initialize neighbor containers
        directNeighbours = new GenericBag<AbstractCell>();
        directNeighbourIDs = new HashSet<Long>();
        lostNeighbourContactInSimSteps = new HashMap<Long, Integer>();
    }
     
    // Assignment of model connector to instantiated cell
    public void setEpisimModelConnector(EpisimModelConnector modelConnector)
    {
   	    if(modelConnector instanceof EpisimTCellMedakaCenterBased3DMC)
   	    {
   		    this.modelConnector = (EpisimTCellMedakaCenterBased3DMC) modelConnector;
   		    this.modelConnector.setCellId((int)getCell().getID());
   		    Double3D loc = cellLocation == null ? cellField.getObjectLocation(getCell()) : cellLocation;
   		
   		    if(loc != null)
   		    {
   			    this.modelConnector.setX(loc.x);
   			    this.modelConnector.setY(loc.y);
   			    this.modelConnector.setZ(loc.z);
   		    }
   	    }
   	    else throw new IllegalArgumentException("Episim Model Connector must be of type: EpisimCenterBased3DMC");
    } 
   
    public EpisimModelConnector getEpisimModelConnector()
    {
   	    return this.modelConnector;
    }
   
    // Result of force calculation
    private class InteractionResult
    {        
        int numhits;
        private Vector3d adhesionForce;
        private Vector3d repulsiveForce;
        private Vector3d chemotacticForce;
                      
        InteractionResult()
        {           
            numhits = 0;
            adhesionForce = new Vector3d(0,0,0);
            repulsiveForce = new Vector3d(0,0,0);
            chemotacticForce = new Vector3d(0,0,0);
        }
    }
   
    // Calculate forces and displacement
    public InteractionResult calculateRepulsiveAdhesiveAndChemotacticForces(Bag neighbours, Double3D thisloc, boolean finalSimStep)
    {
        // check if actual position involves a collision, if so return TRUE, otherwise return FALSE
        // for each collision calc a pressure vector and add it to the other's existing one
        InteractionResult interactionResult = new InteractionResult();           
       
        if (neighbours == null || neighbours.numObjs == 0 || thisloc == null) return interactionResult;
       
        double thisSemiAxisA = getCellWidth()/2;      
        double thisSemiAxisB = getCellHeight()/2;
        double thisSemiAxisC = getCellLength()/2;
                
        Point3d thislocP = new Point3d(thisloc.x, thisloc.y, thisloc.z);
        double totalContactArea = 0;
       
        double numberOfOverlappingNeighbours = 0;
        double cumulativeOverlap = 0;
        
        /*
         * The active motion parameters are set as displacement with units [�m/simstep] in the CBM.
         * To obtain the unit of force [N](*10^-6 in �m), one needs to divide by [s/simstep] and multiply by [kg/s], the latter of which is the unit of friction.
         */
        EpisimTCellMedakaCenterBased3DMC mechModelThis = ((episimmcc.centerbased3d.tcellmedaka.students.EpisimTCellMedakaCenterBased3DMC)this.modelConnector);
        double thisWalkX = mechModelThis.getWalkX();
        double thisWalkY = mechModelThis.getWalkY();
        double thisWalkZ = mechModelThis.getWalkZ();
        
        double frictionMedium = globalParameters.getFrictionMedium();
        int secondsPerSimstep = globalParameters.getNumberOfSecondsPerSimStep();
        
        interactionResult.chemotacticForce.x = thisWalkX*frictionMedium/secondsPerSimstep;
	    interactionResult.chemotacticForce.y = thisWalkY*frictionMedium/secondsPerSimstep;
	    interactionResult.chemotacticForce.z = thisWalkZ*frictionMedium/secondsPerSimstep;
        
        // Loop over cell neighbors
        for(int i=0;i<neighbours.numObjs;i++)
        {
            // Check if the current neighbor exists or is a cell
      	    if (neighbours.objs[i] == null || !(neighbours.objs[i] instanceof AbstractCell)) continue;          
       
            AbstractCell other = (AbstractCell)(neighbours.objs[i]);
            TCellMedakaCenterBased3DModel mechModelOther = (TCellMedakaCenterBased3DModel) other.getEpisimBioMechanicalModelObject();
            if (other != getCell() && mechModelOther != null)
            {   	 
         	    double otherSemiAxisA = mechModelOther.getCellWidth()/2;             
                double otherSemiAxisB = mechModelOther.getCellHeight()/2;
                double otherSemiAxisC = mechModelOther.getCellLength()/2;
                Double3D otherloc = mechModelOther.cellLocation == null ? cellField.getObjectLocation(other) : mechModelOther.cellLocation;
                double dx = cellField.tdx(thisloc.x,otherloc.x); 
                double dy = cellField.tdy(thisloc.y,otherloc.y);
                double dz = cellField.tdz(thisloc.z,otherloc.z); 
             
                Point3d otherlocP = new Point3d(otherloc.x, otherloc.y, otherloc.z);
                
                // Calculate distance from cell's center to its boundary membrane and neighbor[i]'s center to its boundary membrane
                double requiredDistanceToMembraneThis = calculateDistanceToCellCenter(thislocP, otherPosToroidalCorrection(thislocP, otherlocP), 
																											   thisSemiAxisA, thisSemiAxisB, thisSemiAxisC);
                double requiredDistanceToMembraneOther = calculateDistanceToCellCenter(otherlocP, otherPosToroidalCorrection(otherlocP, thislocP), 
																											   otherSemiAxisA, otherSemiAxisB, otherSemiAxisC);            
             
                // Optimal distance - this is simply the sum of the cell-center to boundary membrane distance of both cells
                double optDistScaled = (requiredDistanceToMembraneThis+requiredDistanceToMembraneOther)*globalParameters.getOptDistanceScalingFactor();
                double optDist = (requiredDistanceToMembraneThis+requiredDistanceToMembraneOther);    
          
                /*
                 * Sum overlap to each neighbor and count total number of neighbors (used to obtain average overlap later)
                 */
                double actDist = Math.sqrt(dx*dx+dy*dy+dz*dz); // Euclidean distance
                double overlapUnscaled = optDist - actDist;
                if(overlapUnscaled >= 0)
                {
            	    cumulativeOverlap += overlapUnscaled;
            	    numberOfOverlappingNeighbours++;            	 
                }
             
                // Repulsive forces
                // Calculate only if the difference from the optimal distance is really significant.
                if (optDistScaled-actDist>MIN_OVERLAP_MICRON && actDist > 0) 
                {
            	    // Linear-exponential law according to Pathmanathan et al. 2009
                    /* 
            	     * The response is linear up to some maximum overlap delta > 0, and thereafter rises rapidly.
            	     * The parameter alpha scales the exponent and lies within (0, 1].
            	     * In the limit of large alpha this becomes a hard constraint that the separation can never be less than delta, 
            	     * which is sometimes referred to as a hard-core model.
            	     */
            	 
                    double overlap = optDistScaled - actDist;
            	    double stiffness = globalParameters.getRepulSpringStiffness_N_per_micro_m(); //Standard: 2.2x10^-3 [N m^-1] * 1*10^-6 conversion to micron 
            	    double linearToExpMaxOverlapPerc = globalParameters.getLinearToExpMaxOverlap_perc();
            	    double alpha = 1;
            	 
            	    // If overlap less than hard-core threshold, use linear spring model
            	    double force = overlap <= (optDistScaled*linearToExpMaxOverlapPerc) ? overlap*stiffness : stiffness*(optDistScaled*linearToExpMaxOverlapPerc)*Math.exp(alpha*((overlap/(optDistScaled*linearToExpMaxOverlapPerc))-1));
            	    
            	    interactionResult.repulsiveForce.x += force*dx/actDist;
            	    interactionResult.repulsiveForce.y += force*dy/actDist;
            	    interactionResult.repulsiveForce.z += force*dz/actDist;                                       
                    interactionResult.numhits++;                                                           
                }
             
                // Attraction forces
                // Calculate only if distance between cells falls within a certain range.
                else if(((optDist-actDist) <= -1*MIN_OVERLAP_MICRON) && (actDist < optDist*globalParameters.getOptDistanceAdhesionFact())) 
                {
            	    // Contact area approximated according to Dallon and Othmer 2004
            	    // Calculated for ellipsoids not ellipses
                    double adh_Dist_Fact = globalParameters.getOptDistanceAdhesionFact();
                    double adh_Dist_Perc = globalParameters.getOptDistanceAdhesionFact()-1;
                    double d_membrane_this = requiredDistanceToMembraneThis;
                    double d_membrane_other = requiredDistanceToMembraneOther;
            	    double radius_this_square = Math.pow((adh_Dist_Fact*d_membrane_this),2);
            	    double radius_other_square = Math.pow((adh_Dist_Fact*d_membrane_other),2);
            	    double actDist_square = Math.pow(actDist, 2);
            	    double intercell_gap = actDist - optDist;

            	    double contactArea = (Math.PI/(4*actDist_square))*(2*actDist_square*(radius_this_square+radius_other_square)
            																				  + 2*radius_this_square*radius_other_square
            																				  - Math.pow(radius_this_square, 2)-Math.pow(radius_other_square, 2)
            																				  - Math.pow(actDist_square, 2));         
            	           	
            	    double smoothingFunction = (((-1*adh_Dist_Perc*d_membrane_this) < intercell_gap) && (intercell_gap < (adh_Dist_Perc*d_membrane_this)))? 
            											Math.abs(Math.sin((0.5*Math.PI)*(intercell_gap/(adh_Dist_Perc*d_membrane_this))))
            											: 1;
            	    double adhesionCoefficient = globalParameters.getAdhSpringStiffness_N_per_square_micro_m();//*getAdhesionFactor(other); // if the adhesion factor is not implemented it defaults to 0 --> shuts down adhesion completely!
			 
            	    double adhesion = adhesionCoefficient*(contactArea*smoothingFunction);
            	
            	    interactionResult.adhesionForce.x += adhesion*((-dx)/actDist);
            	    interactionResult.adhesionForce.y += adhesion*((-dy)/actDist);
            	    interactionResult.adhesionForce.z += adhesion*((-dz)/actDist);
                }

                // Obtain cell-cell contact area in the final iteration of the mechanical model
                if(this.modelConnector instanceof episimmcc.centerbased3d.tcellmedaka.students.EpisimTCellMedakaCenterBased3DMC && finalSimStep)
                {
          		    double contactAreaCorrect = 0;
          		    if(actDist < optDist*globalParameters.getOptDistanceAdhesionFact())
          		    {      			
          			    contactAreaCorrect = calculateContactAreaNew(new Point3d(thisloc.x, thisloc.y, thisloc.z),
          						otherPosToroidalCorrection(new Point3d(thisloc.x, thisloc.y, thisloc.z), new Point3d(mechModelOther.getX(), mechModelOther.getY(), mechModelOther.getZ())),
          					dy, thisSemiAxisA, thisSemiAxisB, thisSemiAxisC, otherSemiAxisA, otherSemiAxisB, otherSemiAxisC, requiredDistanceToMembraneThis, requiredDistanceToMembraneOther, actDist, optDist);
          			    contactAreaCorrect = Double.isNaN(contactAreaCorrect) || Double.isInfinite(contactAreaCorrect) ? 0: contactAreaCorrect;
          			    ((episimmcc.centerbased3d.tcellmedaka.students.EpisimTCellMedakaCenterBased3DMC)this.modelConnector).setContactArea(other.getID(), Math.abs(contactAreaCorrect));
             		    totalContactArea += Math.abs(contactAreaCorrect); 
          		    }
          		}
            }      
        }
      
        average_overlap = numberOfOverlappingNeighbours > 0 ? (cumulativeOverlap/numberOfOverlappingNeighbours) : 0;
        return interactionResult;
    }
   
    /*
     *  Function to calculate contact area (e.g. cell-cell).
     *  The contact area is calculated based on the intersecting plane (a circle or ellipse).
     */
    private double calculateContactAreaNew(Point3d posThis, Point3d posOther, double dy, double thisSemiAxisA, double thisSemiAxisB, double thisSemiAxisC, double otherSemiAxisA, double otherSemiAxisB, double otherSemiAxisC, double d_membrane_this, double d_membrane_other, double actDist, double optDistScaled)
    {
   	    double contactArea = 0;
   	    double adh_Dist_Fact = 1;//should be 1 because we want the precise contact area here
      
        final double AXIS_RATIO_THRES = 5;
      
        if (thisSemiAxisA/thisSemiAxisB >= AXIS_RATIO_THRES || otherSemiAxisA/otherSemiAxisB >=AXIS_RATIO_THRES)
        {
		    Rectangle2D.Double rect1 = new Rectangle2D.Double(posThis.x-thisSemiAxisA, posThis.y-thisSemiAxisB, 2*thisSemiAxisA,2*thisSemiAxisB);
			Rectangle2D.Double rect2 = new Rectangle2D.Double(posOther.x-otherSemiAxisA, posOther.y-otherSemiAxisB, 2*otherSemiAxisA, 2*otherSemiAxisB);
			Rectangle2D.Double intersectionRectXY = new Rectangle2D.Double();
			Rectangle2D.Double.intersect(rect1, rect2, intersectionRectXY);
			double contactRadiusXY = intersectionRectXY.height < thisSemiAxisB && intersectionRectXY.height < otherSemiAxisB ? intersectionRectXY.width : intersectionRectXY.height;						
			contactRadiusXY /= 2;
			
			rect1 = new Rectangle2D.Double(posThis.z-thisSemiAxisC, posThis.y-thisSemiAxisB, 2*thisSemiAxisC, 2*thisSemiAxisB);
			rect2 = new Rectangle2D.Double(posOther.z-otherSemiAxisC, posOther.y-otherSemiAxisB, 2*otherSemiAxisC, 2*otherSemiAxisB);
			Rectangle2D.Double intersectionRectZY = new Rectangle2D.Double();
			Rectangle2D.Double.intersect(rect1, rect2, intersectionRectZY);
			double contactRadiusZY = intersectionRectZY.width;
			contactRadiusZY	/=2;
			
			contactArea = Math.PI*contactRadiusXY*contactRadiusZY;
		}
		else
		{
	        double r1 = adh_Dist_Fact*d_membrane_this;
	        double r2 = adh_Dist_Fact*d_membrane_other;
	        double[] semiAxesCellThis = calculateIntersectionEllipseSemiAxes(posThis, posOther, thisSemiAxisA, thisSemiAxisB, thisSemiAxisC);
			double[] semiAxesCellOther = calculateIntersectionEllipseSemiAxes(posOther, posThis, otherSemiAxisA, otherSemiAxisB, otherSemiAxisC);
	      
	        double r1_scaled = (semiAxesCellThis[1]*semiAxesCellThis[0])/r1;
	        double r2_scaled = (semiAxesCellOther[1]*semiAxesCellOther[0])/r2;
	      
	        double actDist_scale = ((r1_scaled)*(1/(r1+r2))+(r2_scaled)*(1/(r1+r2)));
	        double actDist_scaled = actDist*actDist_scale;
	      
            double actDist_square = Math.pow(actDist_scaled, 2);
	        double radius_this_square = Math.pow(r1_scaled,2);
	   	    double radius_other_square = Math.pow(r2_scaled,2);

            contactArea = (Math.PI/(4*actDist_square))*(2*actDist_square*(radius_this_square+radius_other_square)
	   																		+2*radius_this_square*radius_other_square
	   																		-Math.pow(radius_this_square, 2)-Math.pow(radius_other_square, 2)
	   																		-Math.pow(actDist_square, 2));
        }
        return contactArea;
    }
    
    private double[] calculateIntersectionEllipseSemiAxes(Point3d cellCenter, Point3d otherCellCenter, double aAxis, double bAxis, double cAxis)
    {
	    //According to P.P. Klein 2012 on the Ellipsoid and Plane Intersection Equation
		Vector3d directR = new Vector3d((otherCellCenter.x-cellCenter.x), (otherCellCenter.y-cellCenter.y), (otherCellCenter.z-cellCenter.z));
		directR.normalize();
		Vector3d directTemp = new Vector3d(0, 1, 0);
		directTemp.normalize();
		 
		if(directTemp.equals(directR))
		{
		    directTemp = new Vector3d(1, 0, 0);
			directTemp.normalize();
		}
		 
		 Vector3d normalVect = new Vector3d();
		 normalVect.cross(directR, directTemp);
		 normalVect.normalize();
		 
		 Vector3d directS = new Vector3d();		 
		 directS.cross(normalVect, directR);
		 directS.normalize();
		 Matrix3d diagMatrixD = new Matrix3d(1d/aAxis,0,0,0,1d/bAxis,0,0,0,1d/cAxis);
		 double dr_dr = mult(diagMatrixD, directR).dot(mult(diagMatrixD, directR));
		 double ds_ds = mult(diagMatrixD, directS).dot(mult(diagMatrixD, directS));
		 double dr_ds = mult(diagMatrixD, directR).dot(mult(diagMatrixD, directS));
		 
		 double diff_drdr_dsds = dr_dr-ds_ds;
		 
		 double angle = diff_drdr_dsds != 0 ? (0.5*Math.atan((2*dr_ds)/diff_drdr_dsds)) : (Math.PI/4d);
		 
		 double sinAngle = Math.sin(angle);
		 double cosAngle = Math.cos(angle);
		 
		 Vector3d rVect = new Vector3d((cosAngle*directR.x+sinAngle*directS.x),(cosAngle*directR.y+sinAngle*directS.y), (cosAngle*directR.z+sinAngle*directS.z));
		 Vector3d sVect = new Vector3d((cosAngle*directS.x-sinAngle*directR.x),(cosAngle*directS.y-sinAngle*directR.y), (cosAngle*directS.z-sinAngle*directR.z));
		 
		 Vector3d qVect = new Vector3d(0,0,0);
		 
		 dr_dr = mult(diagMatrixD, rVect).dot(mult(diagMatrixD, rVect));
		 ds_ds = mult(diagMatrixD, sVect).dot(mult(diagMatrixD, sVect));
		 double dq_dq = mult(diagMatrixD, qVect).dot(mult(diagMatrixD, qVect));
		 double dq_dr = mult(diagMatrixD, qVect).dot(mult(diagMatrixD, rVect));
		 double dq_ds = mult(diagMatrixD, qVect).dot(mult(diagMatrixD, sVect));		 
		 double dFact = dq_dq - (Math.pow(dq_dr, 2)/dr_dr) -(Math.pow(dq_ds, 2)/ds_ds);
		 return new double[]{Math.sqrt((1-dFact)/dr_dr),Math.sqrt((1-dFact)/ds_ds)};
    }
   
    private Vector3d mult(Matrix3d m, Vector3d v )
    {
	    return new Vector3d((m.m00*v.x + m.m01*v.y+ m.m02*v.z), (m.m10*v.x + m.m11*v.y+ m.m12*v.z), (m.m20*v.x + m.m21*v.y+ m.m22*v.z));
	}
   
    private double getAdhesionFactor(AbstractCell otherCell)
    {
   	    return modelConnector.getAdhesionFactorForCell(otherCell);
    } 
   
	// This function is used both for cell-cell distance calculation and cell-thymus distance
    private double calculateDistanceToCellCenter(Point3d cellCenter, Point3d otherCellCenter, double aAxis, double bAxis, double cAxis)
    {
   	    Point3d intersectionPointEllipsoid = calculateIntersectionPointOnEllipsoid(cellCenter, otherCellCenter, aAxis, bAxis, cAxis);	   
	    return cellCenter.distance(intersectionPointEllipsoid);
	}
    
    private Point3d calculateIntersectionPointOnEllipsoid(Point3d cellCenter, Point3d otherCellCenter, double aAxis, double bAxis, double cAxis)
    {
   	    Vector3d rayDirection = new Vector3d((otherCellCenter.x-cellCenter.x), (otherCellCenter.y-cellCenter.y), (otherCellCenter.z-cellCenter.z));
		rayDirection.normalize();
		//calculates the intersection of a ray with an ellipsoid
		double aAxis_2=aAxis * aAxis;
		double bAxis_2=bAxis * bAxis;
		double cAxis_2=cAxis * cAxis;
		 
	    double a = ((rayDirection.x * rayDirection.x) / (aAxis_2))
	             + ((rayDirection.y * rayDirection.y) / (bAxis_2))
	             + ((rayDirection.z * rayDirection.z) / (cAxis_2));
	  
	    if (a < 0)
	    {
	        System.out.println("Error in optimal Ellipsoid distance calculation"); 
	   	    return null;
	    }
	    
	    double sqrtA = Math.sqrt(a);	 
	    double hit = 1 / sqrtA;
	    double hitsecond = -1*(1 / sqrtA);
	    
	    double linefactor = hit;
	    return new Point3d((cellCenter.x+ linefactor*rayDirection.x),(cellCenter.y+ linefactor*rayDirection.y),(cellCenter.z+ linefactor*rayDirection.z));
    }
    
    // Put cells to new position after calculating all forces   
    public void setPositionRespectingBounds(Point3d cellPosition, boolean setPositionInCellField)
	{	    
	    if(setPositionInCellField)
	    {
	   	    oldCellLocation = cellLocation;
		    cellLocation = new Double3D(cellPosition.x, cellPosition.y, cellPosition.z);
	   	    setCellLocationInCellField(cellLocation);
	    }
	    else
	    {
	   	    oldCellLocation = cellLocation;
	   	    newCellLocation = new Double3D(cellPosition.x, cellPosition.y, cellPosition.z);
	    }
    }
    
    // Initialization of a new simulation step
	public void initNewSimStep()
	{
		if(globalParameters == null)
		{
		    if(ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters() instanceof TCellMedakaCenterBased3DModelGP)
		    {
                globalParameters = (TCellMedakaCenterBased3DModelGP) ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters();
	  		}   	
	  		else throw new GlobalParameterException("Datatype of Global Mechanical Model Parameters does not fit : "+
	  		 			ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters().getClass().getName());
 	 	}
		migrationDistWholeSimStep = 0;
	}
	
    // Forward Euler integration
	public void calculateSimStep(boolean finalSimStep)
	{
        if(finalSimStep)
        {
		    this.modelConnector.resetPairwiseParameters();
			if(modelConnector instanceof episimmcc.centerbased3d.tcellmedaka.students.EpisimTCellMedakaCenterBased3DMC)
			{				
		        ((episimmcc.centerbased3d.tcellmedaka.students.EpisimTCellMedakaCenterBased3DMC) modelConnector).getContactArea().clear();
		  	}
			this.modelConnector.setAverage_overlap(0);
		}
		//according to Pathmanathan et al.2008
		Double3D loc = cellLocation == null ? cellField.getObjectLocation(getCell()) : cellLocation;
		double frictionConstantMedium = globalParameters.getFrictionMedium(); //0.0000004;//Galle, Loeffler Drasdo 2005 epithelial cells 0.4 Ns/m (* 10^-6 for conversion to micron )
		double bm_factor = globalParameters.getTec_biomechanics_factor();
				
		if(loc != null)
		{
		    Bag neighbours = cellField.getNeighborsWithinDistance(loc, 
					getCellWidth()*globalParameters.getMechanicalNeighbourhoodOptDistFact(),
					getCellHeight()*globalParameters.getMechanicalNeighbourhoodOptDistFact(),
					getCellLength()*globalParameters.getMechanicalNeighbourhoodOptDistFact(),					
					true, true);
		    
			InteractionResult interactionResult = calculateRepulsiveAdhesiveAndChemotacticForces(neighbours, loc, finalSimStep);

			// Exclude TSP source cells and TEC+TEClets from being displaced!
			if( !getCell().getEpisimCellBehavioralModelObject().getDiffLevel().toString().equals("DL_TSP_source") && 
				!getCell().getEpisimCellBehavioralModelObject().getDiffLevel().toString().equals("DL_TEC_let") && 
				!getCell().getEpisimCellBehavioralModelObject().getDiffLevel().toString().equals("DL_TEC_soma") )
			{
				
				// Explicit forward Euler integration
				double newX = loc.x + ((DELTA_TIME_IN_SECONDS_PER_EULER_STEP/frictionConstantMedium)*(interactionResult.repulsiveForce.x + interactionResult.adhesionForce.x + interactionResult.chemotacticForce.x));
				double newY = loc.y + ((DELTA_TIME_IN_SECONDS_PER_EULER_STEP/frictionConstantMedium)*(interactionResult.repulsiveForce.y + interactionResult.adhesionForce.y + interactionResult.chemotacticForce.y));
				double newZ = loc.z + ((DELTA_TIME_IN_SECONDS_PER_EULER_STEP/frictionConstantMedium)*(interactionResult.repulsiveForce.z + interactionResult.adhesionForce.z + interactionResult.chemotacticForce.z));

				if(Math.abs(newX-loc.x)> MAX_DISPLACEMENT || Math.abs(newY-loc.y)> MAX_DISPLACEMENT || Math.abs(newZ-loc.z)> MAX_DISPLACEMENT)
				{
					System.out.println("Biomechanical artifact: movement too large! Ignoring move. Try reducing time step.");
				}
				else
				{
				    setPositionRespectingBounds(new Point3d(newX, newY, newZ), false);
				}
			}
			else if (getCell().getEpisimCellBehavioralModelObject().getDiffLevel().toString().equals("DL_TSP_source") ||
					getCell().getEpisimCellBehavioralModelObject().getDiffLevel().toString().equals("DL_TEC_let") )
			{

				// Explicit forward Euler integration
				double newX = loc.x + bm_factor*((DELTA_TIME_IN_SECONDS_PER_EULER_STEP/frictionConstantMedium)*(interactionResult.repulsiveForce.x + interactionResult.adhesionForce.x + interactionResult.chemotacticForce.x));
				double newY = loc.y + bm_factor*((DELTA_TIME_IN_SECONDS_PER_EULER_STEP/frictionConstantMedium)*(interactionResult.repulsiveForce.y + interactionResult.adhesionForce.y + interactionResult.chemotacticForce.y));
				double newZ = loc.z + bm_factor*((DELTA_TIME_IN_SECONDS_PER_EULER_STEP/frictionConstantMedium)*(interactionResult.repulsiveForce.z + interactionResult.adhesionForce.z + interactionResult.chemotacticForce.z));

				if(Math.abs(newX-loc.x)> MAX_DISPLACEMENT || Math.abs(newY-loc.y)> MAX_DISPLACEMENT || Math.abs(newZ-loc.z)> MAX_DISPLACEMENT)
				{
					System.out.println("Biomechanical artifact: movement too large! Ignoring move. Try reducing time step.");
				}
				else
				{
				    setPositionRespectingBounds(new Point3d(newX, newY, newZ), false);
				}
			}
			finalInteractionResult = interactionResult;
		}
	}
	
	// Place cells on coordinates that emerge from mechanical force balance and set other cell values.
	public void finishNewSimStep()
	{
		// Update cell location after force calculation for all cells except "source TSPs"

	    Double3D newCellLocation = cellLocation == null ? cellField.getObjectLocation(getCell()) : cellLocation;		
		modelConnector.setX(newCellLocation.getX());
		modelConnector.setY(newCellLocation.getY());
		modelConnector.setZ(newCellLocation.getZ());
		
  	 	modelConnector.setAverage_overlap(average_overlap);
  	 	
	  	TCellMedakaCenterBased3DModelGP mechModelGP = (TCellMedakaCenterBased3DModelGP) ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters();
   	 	
  	 	if(modelConnector instanceof episimmcc.centerbased3d.tcellmedaka.students.EpisimTCellMedakaCenterBased3DMC)
  	 	{
  	 	    episimmcc.centerbased3d.tcellmedaka.students.EpisimTCellMedakaCenterBased3DMC mc = (episimmcc.centerbased3d.tcellmedaka.students.EpisimTCellMedakaCenterBased3DMC) modelConnector;
  	 		mc.setCellSurfaceArea(getSurfaceArea());
  	 		mc.setCellVolume(getCellVolume());
  	 		mc.setExtCellSpaceVolume(getExtraCellSpaceVolume(mc.getExtCellSpaceMikron()));
  	 		mc.setNumberOfSecondsPerSimStep(mechModelGP.getNumberOfSecondsPerSimStep());
  	 		
  	 		// get the number of lattice sites that each cell corresponds to
  	 		double fieldRes = mc.getDf_resolution();
  	 		CellBoundaries cb = this.getCellBoundariesInMikron(fieldRes);
  	 		
  	 		double startX = cb.getMinXInMikron();
  			double stopX = cb.getMaxXInMikron();
  			double startY = cb.getMinYInMikron();
  			double stopY = cb.getMaxYInMikron();
  			double startZ = cb.getMinZInMikron();
  			double stopZ = cb.getMaxZInMikron();
  			
  			int numberOfLatticeSites = 0;
  			for(double z = startZ; z <= stopZ;)
  			{
  				for(double y = startY; y <= stopY;)
  				{
  					for(double x = startX; x <= stopX;)
  					{
  						if(cb.contains(x, y, z))
  						{
  	   					numberOfLatticeSites+=1;
  						}
  						x+=fieldRes;
  					}
  					y+=fieldRes;
  				}
  				z+=fieldRes;
  			}
  	 		mc.setLatticeCells(numberOfLatticeSites);
  	 		
  	 		/*
  	 		 * Check if cell is inside thymus and if not what is the distance from the thymus' surface.
  	 		 */
  	 		double thymusCenterX = mechModelGP.getThymusCenterX();
  	 		double thymusCenterY = mechModelGP.getThymusCenterY();
  	 		double thymusCenterZ = mechModelGP.getThymusCenterZ();
  	 		double thymusWidth = mechModelGP.getThymusWidth();
  	 		double thymusHeight = mechModelGP.getThymusHeight();
  	 		double thymusLength = mechModelGP.getThymusLength();
  	 		
  	 		mc.setThymusCenterX(thymusCenterX);
  	 		mc.setThymusCenterY(thymusCenterY);
  	 		mc.setThymusCenterZ(thymusCenterZ);
  	 		mc.setDomainWidth(mechModelGP.getWidthInMikron());
  	 		mc.setDomainHeight(mechModelGP.getHeightInMikron());
  	 		mc.setDomainLength(mechModelGP.getLengthInMikron());
  	 		  	 		
  	 		Point3d cellCenter = new Point3d(newCellLocation.getX(), newCellLocation.getY(), newCellLocation.getZ());
  	 		Point3d thymusCenter = new Point3d(thymusCenterX, thymusCenterY, thymusCenterZ);
  	 		  	 		
  	 		boolean isInThymus = checkIfInsideEllipsoid(cellCenter, thymusCenter, thymusWidth/2, thymusHeight/2, thymusLength/2);
  	 		mc.setCellInThymus(isInThymus);

	  	 	double medullaFraction = mechModelGP.getThymusInnerCoreFraction();
  	 		Vector3d distanceFromThymus = calcVectorFromThymus(cellCenter, thymusCenter, thymusWidth/2, thymusHeight/2, thymusLength/2);
  	 		Vector3d distanceFromMedulla = calcVectorFromThymus(cellCenter, thymusCenter, medullaFraction*(thymusWidth/2), medullaFraction*(thymusHeight/2), medullaFraction*(thymusLength/2));
  	 		if (isInThymus)
  	 		{
  	  	 		boolean isInMedulla;
  	  	 		{
  	 			isInMedulla = checkIfInsideEllipsoid(cellCenter, thymusCenter, medullaFraction*(thymusWidth/2), medullaFraction*(thymusHeight/2), (thymusLength/2));
  	  	 		}
  	  	 		mc.setInMedulla(isInMedulla);
  	  	 		mc.setInCortex(!isInMedulla);
  	 		}
  	 		else
  	 		{
  	  	 		mc.setInMedulla(false);
  	  	 		mc.setInCortex(false);
  	 		}
  	 		mc.setThymusDistanceX(distanceFromThymus.x);
  	 		mc.setThymusDistanceY(distanceFromThymus.y);
  	 		mc.setThymusDistanceZ(distanceFromThymus.z);
  	 		mc.setMedullaDistanceX(distanceFromMedulla.x);
  	 		mc.setMedullaDistanceY(distanceFromMedulla.y);
  	 		mc.setMedullaDistanceZ(distanceFromMedulla.z);
  	 		
  	 		// If cell has entered a new cell cycle phase, initialize the new phase length
  	 		if (mc.getPhase_switch())
  	 		{
  	 			double mean_param = mc.getPhase_mean() <= 0 ? 1000 : mc.getPhase_mean() ;
  	 			int step_param = mc.getPhase_steps();
  	 			
  	 			final MersenneTwisterFast random_uniform = new MersenneTwisterFast(System.currentTimeMillis());
  	 			double rate_param = step_param/mean_param;
  	 			double phase_duration = generateErlangDistributedNumber(rate_param, step_param, random_uniform);

  	 			mc.setPhase_duration(phase_duration);
  	 			mc.setPhase_switch(false);
  	 		}
  	 		
  	 		/*
  	 		 * A cell keeps track of the identity of all neighboring cells that it contacted within the last 10 simulation steps.
  	 		 * In models where cell-cell adhesion is calculated dynamically proportionally to the cell-cell contact, it's important
  	 		 * to prevent that random deviations lead to stochastic cell "detachment", essentially resetting the pairwise adhesion to 0.
  	 		 */
	  	 	Set<Long> keySet = new HashSet<Long>();
	 		keySet.addAll(mc.getCellCellAdhesion().keySet());
	 		HashMap<Long, Double> cellCellAdhesion = mc.getCellCellAdhesion();
	 		for(Long key : keySet)
	 		{
	 			if(!directNeighbourIDs.contains(key))
	 			{
	 				int neighbourLostSteps = 0;
	 				if(!lostNeighbourContactInSimSteps.containsKey(key))lostNeighbourContactInSimSteps.put(key, neighbourLostSteps);
	 				else
	 				{
	 					neighbourLostSteps = lostNeighbourContactInSimSteps.get(key) +1;
	 					lostNeighbourContactInSimSteps.put(key, neighbourLostSteps);
	 				}
	 				if(neighbourLostSteps >= globalParameters.getNeighbourLostThres())
	 				{
	 					cellCellAdhesion.remove(key);
	 					lostNeighbourContactInSimSteps.remove(key);
	 				}
	 			}
	 			else
	 			{
	 				if(lostNeighbourContactInSimSteps.containsKey(key))
	 				{
	 					lostNeighbourContactInSimSteps.remove(key);
	 				}
	 			}
	 		}
  	 	}
	}
	
	private double getSurfaceArea()
	{
		//this method was implemented according to Knud Thomsens Approximation for ellipsoids
		final double p = 1.6075d;
		double a = modelConnector.getWidth()/2d;
		double b = modelConnector.getHeight()/2d;
		double c = modelConnector.getLength()/2d;
		
		double axisSum = (Math.pow(a, p)*Math.pow(b, p))+(Math.pow(a, p)*Math.pow(c, p))+(Math.pow(b, p)*Math.pow(c, p));
		double p_root = Math.pow((axisSum/3), (1.0/p));
		
		double result = 4*Math.PI*p_root;		
		return result;
	}
	
	private double getCellVolume()
	{
		double a = modelConnector.getWidth()/2d;
		double b = modelConnector.getHeight()/2d;
		double c = modelConnector.getLength()/2d;		
		double result = (4.0d/3.0d)*Math.PI*a*b*c;		
		return result;
	}
	
	private double getExtraCellSpaceVolume(double extCellSpaceDelta)
	{
		double a = (modelConnector.getWidth()/2d)+extCellSpaceDelta;
		double b = (modelConnector.getHeight()/2d)+extCellSpaceDelta;
		double c = (modelConnector.getLength()/2d)+extCellSpaceDelta;		
		return ((4.0d/3.0d)*Math.PI*a*b*c)-getCellVolume();
	}
   
    @NoExport
    public GenericBag<AbstractCell> getDirectNeighbours()
    {
   	    return directNeighbours;
    }
   
    // Neighbor localization
    private void updateDirectNeighbours()
    {
   	    GenericBag<AbstractCell> neighbours = getCellularNeighbourhood(true);
   	    directNeighbours.clear();
   	    directNeighbourIDs.clear();
   	    Double3D thisloc = this.cellLocation == null ? cellField.getObjectLocation(this) :this.cellLocation;
   	    Point3d thisLocP = new Point3d(thisloc.x, thisloc.y, thisloc.z);

   	    for(int i=0;neighbours != null && i<neighbours.size();i++)
   	    {
  		    AbstractCell actNeighbour = neighbours.get(i);	  		 	
	  		if (actNeighbour != getCell())
	        {
	  		    TCellMedakaCenterBased3DModel mechModelOther = (TCellMedakaCenterBased3DModel) actNeighbour.getEpisimBioMechanicalModelObject();
	      	    Double3D otherloc = mechModelOther.cellLocation == null ? cellField.getObjectLocation(actNeighbour) : mechModelOther.cellLocation;
	      	    Point3d otherlocP = new Point3d(otherloc.x, otherloc.y, otherloc.z);
	      	    double dx = cellField.tdx(thisLocP.x,otherloc.x); 
	            double dy = cellField.tdy(thisLocP.y,otherloc.y);
	            double dz = cellField.tdz(thisLocP.z,otherloc.z);
	           
	            double requiredDistanceToMembraneThis = calculateDistanceToCellCenter(thisLocP, 
							otherPosToroidalCorrection(thisLocP, otherlocP), 
							getCellWidth()/2, getCellHeight()/2, getCellLength()/2);
	            double requiredDistanceToMembraneOther = calculateDistanceToCellCenter(otherlocP, 
							otherPosToroidalCorrection(otherlocP,thisLocP), 
							mechModelOther.getCellWidth()/2, mechModelOther.getCellHeight()/2, mechModelOther.getCellLength()/2);
	          
	            double optDist = (requiredDistanceToMembraneThis+requiredDistanceToMembraneOther); 
	            double actDist = Math.sqrt(dx*dx+dy*dy+dz*dz);        
		        if(actDist <= globalParameters.getDirectNeighbourhoodOptDistFact()*optDist)
		        {
		      	    directNeighbours.add(actNeighbour);
		      	    directNeighbourIDs.add(actNeighbour.getID());	      	 
		        }
            }
        }
   	}
   
    // Correction for toroidal domain
    public Point3d otherPosToroidalCorrection(Point3d thisCell, Point3d otherCell)
    {
	    double height = cellField.height;
	    double width = cellField.width;
	    double length = cellField.length;
	    double otherZ = -1;
	    double otherY = -1;
	    double otherX = -1;
	    if (Math.abs(thisCell.z-otherCell.z) <= length / 2) otherZ = otherCell.z;
	    else
	    {
	   	    otherZ = thisCell.z > otherCell.z ? (otherCell.z+length): (otherCell.z-length);
	    }
	    if (Math.abs(thisCell.y-otherCell.y) <= height / 2) otherY = otherCell.y;
	    else
	    {
	   	    otherY = thisCell.y > otherCell.y ? (otherCell.y+height): (otherCell.y-height);
	    }
	    if (Math.abs(thisCell.x-otherCell.x) <= width / 2) otherX = otherCell.x;
	    else
	    {
	   	    otherX = thisCell.x > otherCell.x ? (otherCell.x+width): (otherCell.x-width);
	    }
	    return new Point3d(otherX, otherY, otherZ);
    }
   
    public void initialisationGlobalSimStep()
    {
   	    newGlobalSimStep(Long.MIN_VALUE, null);
    }
   
    /*
     * Biomechanical simulation step
     */
    protected void newGlobalSimStep(long simStepNumber, SimState state)
    {
        final MersenneTwisterFast random = state!= null ? state.random : new MersenneTwisterFast(System.currentTimeMillis());
   	    final GenericBag<AbstractCell> allCells = new GenericBag<AbstractCell>(); 
   	    allCells.addAll(TissueController.getInstance().getActEpidermalTissue().getAllCells());

   	    // Get the biomechanical global parameters
   	    TCellMedakaCenterBased3DModelGP mechModelGP = (TCellMedakaCenterBased3DModelGP) ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters();

        double numberOfSeconds = DELTA_TIME_IN_SECONDS_PER_EULER_STEP;   	
   	    numberOfSeconds = ((TCellMedakaCenterBased3DModelGP)ModelController.getInstance().getEpisimBioMechanicalModelGlobalParameters()).getNumberOfSecondsPerSimStep();
   	    double numberOfIterationsDouble = 1; 
   	    if(DELTA_TIME_IN_SECONDS_PER_EULER_STEP > numberOfSeconds)
   	    {
   		    DELTA_TIME_IN_SECONDS_PER_EULER_STEP = numberOfSeconds;   		
   	    }
   	    else if(numberOfSeconds > DELTA_TIME_IN_SECONDS_PER_EULER_STEP)
   	    {
   		    DELTA_TIME_IN_SECONDS_PER_EULER_STEP = numberOfSeconds > DELTA_TIME_IN_SECONDS_PER_EULER_STEP_MAX ? DELTA_TIME_IN_SECONDS_PER_EULER_STEP_MAX : numberOfSeconds;   		   		
   	    }
   	    numberOfIterationsDouble = (numberOfSeconds/DELTA_TIME_IN_SECONDS_PER_EULER_STEP); //according to Pathmanathan et al.2008
   	    final int numberOfIterations = ((int)numberOfIterationsDouble);
   	    boolean parallelizationOn = EpisimProperties.getProperty(EpisimProperties.SIMULATION_PARALLELIZATION) == null || EpisimProperties.getProperty(EpisimProperties.SIMULATION_PARALLELIZATION).equalsIgnoreCase(EpisimProperties.ON);
	    boolean cutOffStop = false;
	    
        // Repeat mechanical calculation until the maximum number of steps has been reached or the cells are in steady state (cutoff)
	    for(int i = 0; i<numberOfIterations; i++)
        {
   		    allCells.shuffle(random);
   		    final int totalCellNumber = allCells.size();
   		    if(cutOffStop)
   		    {
   			    i = (numberOfIterations-1);
            }
   		    final int iterationNo = i;
     		// Loop when parallelization active
    		if(parallelizationOn)
    		{
	   		    Loop.withIndex(0, totalCellNumber, new Loop.Each()
	   		    {
	                public void run(int n)
	                {
	                    TCellMedakaCenterBased3DModel cellBM = ((TCellMedakaCenterBased3DModel)allCells.get(n).getEpisimBioMechanicalModelObject()); 
			   			if(iterationNo == 0) cellBM.initNewSimStep();
			   			cellBM.calculateSimStep((iterationNo == (numberOfIterations-1)));				   			
	                }
	            });  		
   		    }
   		    // Loop when no parallelization
   		    else
   		    {
   			    for(int cellNo = 0; cellNo < totalCellNumber; cellNo++)
   			    {
	   			    TCellMedakaCenterBased3DModel cellBM = ((TCellMedakaCenterBased3DModel)allCells.get(cellNo).getEpisimBioMechanicalModelObject()); 
	   			    if(i == 0) cellBM.initNewSimStep();
	   			    cellBM.calculateSimStep((i == (numberOfIterations-1)));		   			
	   		    }
   		    }
   		    double cumulativeMigrationDistance = 0;
   		    double migrationDistanceThisStep = 0;
   		    for(int cellNo = 0; cellNo < totalCellNumber; cellNo++)
   		    {
   			    TCellMedakaCenterBased3DModel cellBM = ((TCellMedakaCenterBased3DModel)allCells.get(cellNo).getEpisimBioMechanicalModelObject());
   			    if(cellBM.newCellLocation!=null)
   			    {
   				    if(cellBM.oldCellLocation != null)
   				    {
   					    migrationDistanceThisStep = cellBM.oldCellLocation.distance(cellBM.newCellLocation);
   					    cellBM.migrationDistWholeSimStep += migrationDistanceThisStep;
   				    }	
   				    if(cellBM.newCellLocation != null)cellBM.setCellLocationInCellField(cellBM.newCellLocation);
         			else if(cellBM.cellLocation != null)cellBM.setCellLocationInCellField(cellBM.cellLocation);
   			    }
   			    if(iterationNo == (numberOfIterations-1))
   			    {
   				    // Finish the biomechanical iteration --> update cell positions and eye radius
   				    cellBM.updateDirectNeighbours();
   				    cellBM.finishNewSimStep();
   			    }
   			    cumulativeMigrationDistance+=migrationDistanceThisStep;
   			    migrationDistanceThisStep=0;
   		    }   		
   		    // Cut-off value of average migration to interrupt biomechanical simulation
   		    if(totalCellNumber>0
   				&& mechModelGP.getNumberOfSecondsPerSimStep() > 0
   				&& (cumulativeMigrationDistance/totalCellNumber) < (mechModelGP.getMinAverageMigrationMikron()/(mechModelGP.getNumberOfSecondsPerSimStep()/36) ))
   		    {
   			    cutOffStop=true;
   		    }
   	    }
    }
		
	public int hitsOtherCell(){ return finalInteractionResult.numhits; }
	
	// Get neighbors
	private GenericBag<AbstractCell> getCellularNeighbourhood(boolean toroidal) 
	{
        Double3D loc = cellLocation == null? cellField.getObjectLocation(getCell()):cellLocation;
		Bag neighbours = cellField.getNeighborsWithinDistance(loc, 
																	getCellWidth()*globalParameters.getMechanicalNeighbourhoodOptDistFact(),
																	getCellHeight()*globalParameters.getMechanicalNeighbourhoodOptDistFact(),
																	getCellLength()*globalParameters.getMechanicalNeighbourhoodOptDistFact(),
																	toroidal, true);
		GenericBag<AbstractCell> neighbouringCells = new GenericBag<AbstractCell>();
		for(int i = 0; i < neighbours.size(); i++)
		{
		    if(neighbours.get(i) instanceof AbstractCell && ((AbstractCell) neighbours.get(i)).getID() != this.getCell().getID())
		    {
			    neighbouringCells.add((AbstractCell)neighbours.get(i));
			}
		}
		return neighbouringCells;
	}
	
	public double generateErlangDistributedNumber(double rate_parameter, int step_parameter, MersenneTwisterFast random_uniform)
	{
		
		double erlang_product = 1;
		double erlang_random_number;
		for(int i = 0; i < step_parameter; i++)
		{
			erlang_product = erlang_product*random_uniform.nextDouble();
		}
		erlang_random_number = (-1/rate_parameter)*Math.log(erlang_product);
		return erlang_random_number;
	}

	public boolean checkIfInsideEllipsoid(Point3d cellCenter, Point3d thymusCenter, double thymusSemiWidth, double thymusSemiHeight, double thymusSemiLength)
	{
		boolean isInside = false;
		
		double ellipseEquationCalc = Math.pow((cellCenter.x-thymusCenter.x)/thymusSemiWidth, 2) 
			       + Math.pow((cellCenter.y-thymusCenter.y)/thymusSemiHeight, 2);

		if (ellipseEquationCalc <= 1 && cellCenter.y <= thymusCenter.y && (cellCenter.z <= thymusCenter.z + thymusSemiLength && cellCenter.z >= thymusCenter.z - thymusSemiLength ) )
		{
			isInside = true;
		}
		
		return isInside;
	}
	
	public Vector3d calcVectorFromThymus(Point3d cellCenter, Point3d thymusCenter, double thymusSemiWidth, double thymusSemiHeight, double thymusSemiLength)
	{
		Vector3d directR;
		double x_comp = 0;
		double y_comp = 0;
		double z_comp = 0;
		
		final MersenneTwisterFast random_uniform = new MersenneTwisterFast(System.currentTimeMillis());
		double eps = 1E-30;
		double ellipseXYcondition = Math.sqrt( Math.pow((cellCenter.x-thymusCenter.x)/thymusSemiWidth, 2) + Math.pow((cellCenter.y-thymusCenter.y)/thymusSemiHeight, 2) );

		if ( Math.abs(cellCenter.x - thymusCenter.x) > thymusSemiWidth ) // If beyond width of ellipse
		{
			x_comp = cellCenter.x < thymusCenter.x ? cellCenter.x - (thymusCenter.x - thymusSemiWidth) : cellCenter.x - (thymusCenter.x + thymusSemiWidth) ;
		}
		
		if ( Math.abs(cellCenter.z - thymusCenter.z) > thymusSemiLength ) // If in front or behind cylinder
		{
			z_comp = cellCenter.z < (thymusCenter.z - thymusSemiLength) ? cellCenter.z - (thymusCenter.z - thymusSemiLength) : cellCenter.z - (thymusCenter.z + thymusSemiLength) ;
		}
		
		if ( cellCenter.y > thymusCenter.y ) // If above half-ellipse
		{
			y_comp = cellCenter.y-thymusCenter.y;
		}
		else if ( ellipseXYcondition > 1 ) // If beyond local radius, move towards local ellipsoidal intersection point
		{
			Point3d thymusRayIntersectionPoint = calculateIntersectionPointOnEllipsoid(cellCenter, thymusCenter, thymusSemiWidth, thymusSemiHeight, thymusSemiLength);
			x_comp = cellCenter.x-thymusRayIntersectionPoint.x;
			y_comp = cellCenter.y-thymusRayIntersectionPoint.y;
		}
		
		directR = new Vector3d(x_comp, y_comp, z_comp);
		
		// Add a small random movement if the cell if the resulting vector is (0, 0, 0) to prevent division by zero in downstream calculations.
		if ( directR.equals( new Vector3d (0,0,0) ) )
		{
			directR.z = random_uniform.nextDouble() > 0.5 ? eps : -eps;
		}
		return directR;
	}
	
	// Used to determine if mechanical calculation is in steady state --> When average migration < threshold
    public double getMigrationDistPerSimStep()
    {
        return migrationDistWholeSimStep;
    }

    // ------------------------------------------
    // Other methods that must be implemented
    // ------------------------------------------
    
	@CannotBeMonitored
	@NoExport
	public double getX()
	{
		return cellLocation != null ? cellLocation.x : modelConnector == null ? 0 : modelConnector.getX();
	}
	
	@CannotBeMonitored
	@NoExport
	public double getY()
	{
	    return cellLocation != null ? cellLocation.y : modelConnector == null ? 0 : modelConnector.getY();
	}
	
	@CannotBeMonitored
	@NoExport
	public double getZ()
	{
	    return cellLocation != null ? cellLocation.z : modelConnector == null ? 0 : modelConnector.getZ();
	}
	
	protected void clearCellField()
	{
	    if(!cellField.getAllObjects().isEmpty())
	    {
	   	    cellField.clear();
	    }
	}
	
	protected void resetCellField()
    {
	    if(!cellField.getAllObjects().isEmpty())
	    {
	   	    cellField.clear();
	    }
	    cellField = new Continuous3DExt(FIELD_RESOLUTION_IN_MIKRON / 1.5, 
				TissueController.getInstance().getTissueBorder().getWidthInMikron(), 
				TissueController.getInstance().getTissueBorder().getHeightInMikron(),
				TissueController.getInstance().getTissueBorder().getLengthInMikron());
	}
	
	public void removeCellFromCellField()
	{
	   cellField.remove(this.getCell());
	}
	
	public void setCellLocationInCellField(Double3D location)
	{
	    cellField.setObjectLocation(this.getCell(), location);
	    this.cellLocation = new Double3D(location.x, location.y, location.z);
	    if(modelConnector!=null)
	    {
	   	    modelConnector.setX(location.x);
	   	    modelConnector.setY(location.y);
	   	    modelConnector.setZ(location.z);
	    }
	}
		
	public Double3D getCellLocationInCellField() 
	{	   
	    return new Double3D(getX(), getY(), getZ());
	}

	protected Object getCellField() 
	{	  
	    return cellField;
	}

	// Function used to get cell outline for hybrid discrete-continuous models
    @CannotBeMonitored
    @NoExport  
    public CellBoundaries getCellBoundariesInMikron(double sizeDelta) 
    {
 	    Double3D fieldLocMikron = getCellLocationInCellField();
 	    Vector3d minVector = null;
 	    Vector3d maxVector = null;
 	    double width = getCellWidth()+sizeDelta;
 	    double height = getCellHeight()+sizeDelta;
 	    double length = getCellLength()+sizeDelta;
 	  
 	    minVector = new Vector3d((fieldLocMikron.x-width/2d), (fieldLocMikron.y-height/2d), (fieldLocMikron.z-length/2d));
 	    maxVector = new Vector3d((fieldLocMikron.x+width/2d), (fieldLocMikron.y+height/2d), (fieldLocMikron.z+length/2d));
 	    Transform3D trans = new Transform3D();
 	 
 	    trans.setTranslation(new Vector3d(fieldLocMikron.x, fieldLocMikron.y, fieldLocMikron.z));
 	    trans.setScale(new Vector3d(width/height, height / height, length/height));
 	    return new CellBoundaries(new Ellipsoid(trans, height/2d), minVector, maxVector);
    }

    public double getStandardCellHeight()
    {
	    return standardCellHeight;
    }

    public void setStandardCellHeight(double val)
    {
   	    this.standardCellHeight = val;	   
    }

    public double getStandardCellWidth()
    {
	    return this.standardCellWidth;
    }
   
    public void setStandardCellWidth(double val)
    {
   	    this.standardCellWidth = val;
    }

    public double getStandardCellLength()
    {
	    return this.standardCellLength;
    }
   
    public void setStandardCellLength(double val)
    {
   	    this.standardCellLength = val;
    }	

    //------------------------------------------------------------------------------------------------------------------------------
    // Abstract methods that must be implemented, but are not needed here
    //------------------------------------------------------------------------------------------------------------------------------
    
    public double getCellHeight() { return modelConnector == null ? 0 : modelConnector.getHeight(); }	
	public double getCellWidth()  { return modelConnector == null ? 0 : modelConnector.getWidth(); }	
	public double getCellLength() { return modelConnector == null ? 0 : modelConnector.getLength(); }
	
	public void setCellHeight(double cellHeight) { if(modelConnector!=null) modelConnector.setHeight(cellHeight > 0 ? cellHeight : getCellHeight()); }	
	public void setCellWidth(double cellWidth) { if(modelConnector!=null) modelConnector.setWidth(cellWidth > 0 ? cellWidth : getCellWidth()); }	
	public void setCellLength(double cellLength) { if(modelConnector!=null) modelConnector.setLength(cellLength > 0 ? cellLength : getCellLength()); }
    
    public void newSimStep(long simstepNumber){}
    
    protected void newSimStepGloballyFinished(long simStepNumber, SimState state){}
    
    @CannotBeMonitored
    @NoExport
 	public EpisimCellShape<Shape3D> getPolygonNucleus(EpisimDrawInfo<TransformGroup> info) {
 		//not yet needed
 		return new Episim3DCellShape<Shape3D>(new Shape3D());
 	}
    
    @CannotBeMonitored
    @NoExport
 	public EpisimCellShape<Shape3D> getPolygonNucleus() {
 		//not yet needed
 		return new Episim3DCellShape<Shape3D>(new Shape3D());
 	}
    
    @CannotBeMonitored
    @NoExport
    public EpisimCellShape<Shape3D> getPolygonCell() {
 		//not yet needed
 		return new Episim3DCellShape<Shape3D>(new Shape3D());
 	}
    
    @CannotBeMonitored
    @NoExport
 	public EpisimCellShape<Shape3D> getPolygonCell(EpisimDrawInfo<TransformGroup> info) {
 		//not yet needed
 		return new Episim3DCellShape<Shape3D>(new Shape3D());
 	}
}