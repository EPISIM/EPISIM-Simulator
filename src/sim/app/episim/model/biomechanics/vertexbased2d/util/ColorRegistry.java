package sim.app.episim.model.biomechanics.vertexbased2d.util;

import java.awt.Color;


public class ColorRegistry {

	public static final Color CELL_FILL_COLOR = Color.WHITE;//new Color(246, 123, 123);
	public static final Color CELL_FILL_COLOR_PROLIFERATING = new Color(200, 97, 97);
	
	public static final Color CELL_FILL_COLOR_ATTACHED_BASALLAYER = new Color(0, 30, 140);
	public static final Color CELL_FILL_COLOR_NEIGHBOUR_ATTACHED_BASALLAYER = new Color(110, 140, 240);
	
	public static final Color VERTEX_ATTACHED_TO_BASALLAYER = new Color(125, 255, 10);
	
	
	
	public static final Color CELL_BORDER_COLOR = new Color(218,7,0);
	public static final Color BASAL_LAYER_COLOR = new Color(250, 90, 30);
	public static final Color BACKGROUND_COLOR = new Color(255, 255, 255); //new Color(0,0,0);
	
}
