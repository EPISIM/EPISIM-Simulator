package sim.app.episim.model.biomechanics.vertexbased2d.geom;





public interface VertexChangeListener {
	
	void handleVertexChangeEvent(VertexChangeEvent event);

}
