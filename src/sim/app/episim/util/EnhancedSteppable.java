package sim.app.episim.util;

import sim.engine.Steppable;


public interface EnhancedSteppable extends Steppable {
	
	double getInterval();
}
