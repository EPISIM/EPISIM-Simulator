package sim.app.episim.util;


public interface ByteArrayWriteListener {

	public void textWasWritten(WriteEvent event);
}
