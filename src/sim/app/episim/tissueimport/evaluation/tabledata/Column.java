package sim.app.episim.tissueimport.evaluation.tabledata;

public interface Column {
	String getColumnName();
}
