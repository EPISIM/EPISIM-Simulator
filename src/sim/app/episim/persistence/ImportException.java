package sim.app.episim.persistence;

public class ImportException extends Exception {

	public ImportException() {
		
	}

	public ImportException(String message) {
		super(message);
		
	}

	public ImportException(Throwable cause) {
		super(cause);
		
	}

	public ImportException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public ImportException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause);
		
	}

}
