package sim.app.episim.visualization;

import java.awt.Color;


public class NucleusEllipse extends AbstractCellEllipse{
		
		public NucleusEllipse(int id, double x, double y, double majorAxis, double minorAxis, double height, double width, double orientationInDegrees, double area, double perimeter, double distanceToBL, Color c){
			super(id,  x,  y,  majorAxis,  minorAxis,  height,  width,  orientationInDegrees,  area, perimeter, distanceToBL,   c);
			
		}

	

}
