package sim.app.episim.visualization.threedim;

import java.awt.Color;


public abstract class Optimized3DVisualization {
	
	public static final Color backgroundColor = new Color(255,240,250);
	public static final Color simulationBoxColor = new Color(50,50,50);
	public static final Color basementMembraneColor = new Color(230,130,130);

}
