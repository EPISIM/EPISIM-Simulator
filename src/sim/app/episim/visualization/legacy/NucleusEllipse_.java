package sim.app.episim.visualization.legacy;

import sim.app.episim.tissueimport.xmlread.ImportedNucleusData;

public class NucleusEllipse_ extends AbstractCellEllipse_ {
	public NucleusEllipse_(ImportedNucleusData nucleusData, double micrometerPerPixel) {
		super(nucleusData, micrometerPerPixel);
	}

}
