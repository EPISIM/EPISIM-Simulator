package episiminterfaces.calc.marker;


public interface SingleCellObserver {
	
	void observedCellHasChanged();

}
