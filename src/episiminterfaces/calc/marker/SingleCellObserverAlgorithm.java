package episiminterfaces.calc.marker;



public interface SingleCellObserverAlgorithm {
	
	void addSingleCellObserver(long[] associatedCalculationHandlerIds, SingleCellObserver observer);
	//void removeSingleCellObserver(SingleCellObserver observer);

}
