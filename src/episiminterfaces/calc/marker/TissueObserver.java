package episiminterfaces.calc.marker;


public interface TissueObserver {
	
	void observedTissueHasChanged();

}
