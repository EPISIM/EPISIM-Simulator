package episiminterfaces.calc;


public interface CalculationCallBack {
	
	void calculate(long simStep);

}
