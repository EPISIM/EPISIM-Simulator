package episiminterfaces.calc;


public interface CellColoringConfigurator extends java.io.Serializable{
	
	String[] getArithmeticExpressionColorR();
	String[] getArithmeticExpressionColorG();
	String[] getArithmeticExpressionColorB();
}
