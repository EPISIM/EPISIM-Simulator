package episiminterfaces;

import episimmcc.EpisimModelConnector;

public interface EpisimCellBehavioralModelExt extends EpisimCellBehavioralModel {	
	
	EpisimModelConnector getEpisimModelConnector();

}
