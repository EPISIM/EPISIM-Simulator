package episiminterfaces;

import java.awt.geom.Rectangle2D;

import sim.util.Double2D;


public interface EpisimPortrayal {
	String getPortrayalName();
	Rectangle2D.Double getViewPortRectangle();
}
