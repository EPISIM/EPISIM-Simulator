package episiminterfaces;


public interface StandardMembraneGP {
	double getBasalOpening_mikron();
 	void setBasalOpening_mikron(double val);
 	
 	double getBasalPeriod_mikron();
 	void setBasalPeriod_mikron(double val);
 	
 	double getBasalYDelta_mikron();
 	void setBasalYDelta_mikron(double val);
 	
 	double getBasalAmplitude_mikron();
 	void setBasalAmplitude_mikron(double val);
}
