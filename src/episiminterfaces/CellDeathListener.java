package episiminterfaces;

import sim.app.episim.model.AbstractCell;


public interface CellDeathListener {

	public void cellIsDead(AbstractCell cell);
	
}
