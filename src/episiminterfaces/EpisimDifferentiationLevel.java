package episiminterfaces;

public interface EpisimDifferentiationLevel {	
	
	String name();
	int ordinal();
}
