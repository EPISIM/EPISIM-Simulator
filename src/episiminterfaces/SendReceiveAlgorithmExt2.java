package episiminterfaces;

public interface SendReceiveAlgorithmExt2 extends SendReceiveAlgorithmExt {
	double senseAVG(String ecDiffusionFieldName, EpisimCellBehavioralModel cell);	
}
